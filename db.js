const { Sequelize, DataTypes } = require('sequelize');

// 从环境变量中读取数据库配置
const { MYSQL_USERNAME, MYSQL_PASSWORD, MYSQL_ADDRESS = '' } = process.env;

const [host, port] = MYSQL_ADDRESS.split(':');
// 线上
const sequelize = new Sequelize('nmb', MYSQL_USERNAME, 'CS@tzz1234', {
    host,
    port,
    logging: true,
    timezone: '+08:00', //东八时区
    dialect: 'mysql' /* one of 'mysql' | 'mariadb' | 'postgres' | 'mssql' */,
});

// 线下
// var sequelize = new Sequelize('test', 'root', 'CS@tzz1234', {
//     host: '10.138.27.68',
//     dialect: 'mysql',
//     port: 8306,
//     pool: {
//         max: 5,
//         min: 0,
//         idle: 30000
//     }
// });

// 线下
// var sequelize = new Sequelize('nmb', 'root', 'CS@tzz1234', {
//     host: 'sh-cynosdbmysql-grp-349gbtga.sql.tencentcdb.com',
//     dialect: 'mysql',
//     port: 28931,
//     logging: true,  // 将sql输出到控制台
//     timezone: '+08:00', //东八时区
//     pool: {
//         max: 5,
//         min: 0,
//         idle: 30000
//     }
// });

const ID_TYPE = Sequelize.STRING(50);

function defineModel(name, attributes, others) {
    var attrs = {};
    for (let key in attributes) {
        let value = attributes[key];
        if (typeof value === 'object' && value['type']) {
            value.allowNull = value.allowNull || false;
            attrs[key] = value;
        } else {
            attrs[key] = {
                type: value,
                allowNull: false
            };
        }
    }
    if (!attrs.id) {
        attrs.id = {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        };
    }
    // attrs.id = {
    //     type: Sequelize.INTEGER,
    //     primaryKey: true,
    //     autoIncrement: true
    // };
    // attrs.createdAt = {
    //     type: Sequelize.BIGINT,
    //     allowNull: false
    // };
    // attrs.updatedAt = {
    //     type: Sequelize.BIGINT,
    //     allowNull: false
    // };
    return sequelize.define(name, attrs, {
        tableName: name,
        timestamps: false,
        hooks: {
            beforeValidate: function (obj) {
                let now = Date.now();
                if (obj.isNewRecord) {
                    // if (!obj.id) {
                    //     obj.id = generateId();
                    // }
                    // obj.createdAt = now;
                    // obj.updatedAt = now;
                } else {
                    // obj.updatedAt = Date.now();
                }
            }
        },
        ...others
    });
}

// // 定义数据模型
// const Counter = sequelize.define('Counteraa', {
//     count: {
//         type: DataTypes.INTEGER,
//         allowNull: false,
//         defaultValue: 1,
//     },
// });

// // 数据库初始化方法
// async function init() {
//     await Counter.sync({ alter: true });
// }

// 导出初始化方法和模型
// module.exports = {
//     init,
//     Counter,
// };


module.exports = {
    sequelize,
    defineModel,
};