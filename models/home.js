/**
 * 首页
 */
const {sequelize} = require('../db');
const {customWhereSql, formatTimer} = require('../util/util.js');
const zhaoPinStatus = require('./zhaoPinStatus.js');
const searchName = require('./searchName.js');
module.exports = {
    // 获取全部数据
    async getAllList(req, res, next) {
        let {page, pageSize, code, typeCode, seachName, userId=''} = req.query;
        pageSize = pageSize || 15;
        page = page || 1;
        let limitStar = page * pageSize - pageSize;
        // 旧的查询全部的sql
        // let sqlStr = `(select id, workDescribe as title, type, formattedAddress, phone, 1 as busnessType, FROM_UNIXTIME(date/1000,'%c月%e日 %h:%m') as time, date from recruitment ${customWhereSql(req.query)})
        // UNION ALL
        // (select id, introduce as title, type, formattedAddress, phone, 2 as busnessType, FROM_UNIXTIME(date/1000,'%c月%e日 %h:%m') as time, date from findJob ${customWhereSql(req.query)})
        // UNION ALL
        // (select id, title, type, formattedAddress, phone, 3 as busnessType, FROM_UNIXTIME(date/1000,'%c月%e日 %h:%m') as time, date from market ${customWhereSql(req.query)})
        // UNION ALL
        // (select id, showName as title, shopType as type, formattedAddress, phone, 4 as busnessType, FROM_UNIXTIME(date/1000,'%c月%e日 %h:%m') as time, date from shop ${customWhereSql(req.query)}) order by date desc limit ${limitStar}, ${pageSize}`;
        
        let status = await zhaoPinStatus.getZhaoPinStatus();
        let sqlStr = '';
        let str = '';
        if (code) {
            // 其他
            if (code == '222222') {
                str = "and adcode not in('130183', '130522', '130129')";
            }else if (code == '111111') {
                // 全部
                str = '';
            }
            else {
                str = "and adcode = " + code;
            }
        }
        if (typeCode && typeCode != '111111') {
            str += ' and type=' + typeCode;
        }

        if (seachName) {
            str += `and workDescribe like '%${seachName}%'`;
            // searchName.insertsearchName(userId, seachName);
        }

        if (status && status == 2) {
            sqlStr = `select id, workDescribe as title, userId, type, tags, browseNum, formattedAddress, toping, phone, closed, 1 as busnessType, DATE_FORMAT(date, '%c月%e日 %H:%i') as time, date from recruitment where examineStatus=3 and type != 1 and type != 2 ${str} order by date desc limit ${limitStar}, ${pageSize}`
        }
        else {
            sqlStr = `select id, workDescribe as title, userId, type, tags, browseNum, formattedAddress,toping, phone, closed, 1 as busnessType, DATE_FORMAT(date, '%c月%e日 %H:%i') as time, date from recruitment where examineStatus=3 ${str} order by concat(toping,date) desc limit ${limitStar}, ${pageSize}`
        }
        // 按距离查询
        // select *, ROUND(6378.138*2*ASIN(SQRT(POW(SIN((37.666274*PI()/180-latitude*PI()/180)/2),2)+COS(37.666274*PI()/180)*COS(latitude*PI()/180)*POW(SIN((114.385605*PI()/180-longitude*PI()/180)/2),2)))*1000) AS juli from recruitment order by juli asc
        
        // 多个用not in (2, 3)
        console.log(sqlStr);
        sequelize.query(sqlStr, {type: sequelize.QueryTypes.SELECT})
        .then(result => {
            let len = result.length;
            for (let i = 0; i < len; i++) {
                result[i].formattedAddress = result[i].formattedAddress.replace('河北省石家庄市', '');
                result[i].formattedAddress = result[i].formattedAddress.replace('河北省邢台市', '');
                let item = result[i];
                let timer = formatTimer(item.date);
                if (timer) {
                    item.time  = timer;
                }
                let {type, tags} = item;
                if (tags) {
                    result[i].tags = tags.split(',');
                }
                if (type == 1) {
                    result[i].title = '【招工】' + result[i].title;
                }
                if (type == 2) {
                    result[i].title = '【找活】' + result[i].title;
                }
                if (type == 3) {
                    result[i].title = '【保洁】' + result[i].title;
                }
                if (type == 4) {
                    result[i].title = '【求购】' + result[i].title;
                }
                if (type == 5) {
                    result[i].title = '【出售】' + result[i].title;
                }
                if (type == 6) {
                    result[i].title = '【搬家】' + result[i].title;
                }
                if (type == 7) {
                    result[i].title = '【出租】' + result[i].title;
                }
                if (type == 8) {
                    result[i].title = '【家政】' + result[i].title;
                }
                if (type == 9) {
                    result[i].title = '【本地农产品】' + result[i].title;
                }
                if (type == 10) {
                    result[i].title = '【美食店铺】' + result[i].title;
                }
                if (type == 11) {
                    result[i].title = '【二手】' + result[i].title;
                }
                if (type == 12) {
                    result[i].title = '【求租】' + result[i].title;
                }
            }
            res.send({
                code: 200,
                msg: '操作成功',
                data: result
            });
        }).catch(err => {
            res.send({
                code: 400,
                msg: '操作失败',
                data: ''
            });
            console.log('获取全部数据失败getAllList：', err);
        });
    },
    /**
     * 获取找活列表
     */
    getFindJobList(req, res, next) {
        let {page, pageSize} = req.query;
        let limitStar = page * pageSize - pageSize;
        let sql = `select id, introduce as title, type, formattedAddress, phone, 2 as busnessType, FROM_UNIXTIME(date/1000,'%c月%e日 %h:%m') as time, date 
            from findJob ${customWhere(req.query)}
            order by date desc limit ${limitStar}, ${pageSize}`;
        // req.query.
        pool.getConnection(function (err, connection) {
            connection.query(sql, function (err, result) {
                if (err) {
                    res.json({
                        code: 400,
                        msg: '操作失败',
                        data: err
                    });
                } else {
                    res.json({
                        code: 200,
                        msg: '操作成功',
                        data: result
                    });
                }
                connection.release();
            });
        });
    },
};