const {defineModel, sequelize} = require('../db');
const {DataTypes } = require("sequelize");
const {timeString, timeFormat} = require('../util/util.js');
const moment = require('moment');
const user = require('./user.js');

module.exports = {
    telPhone: defineModel('telPhone', {
        content: {
            type: DataTypes.TEXT,
            comment: '拨打的信息'
        },
        userId: {
            type: DataTypes.INTEGER,
            comment: '用户ID'
        },
        contentId: {
            type: DataTypes.STRING,
            comment: '拨打来源内容的id'
        },
        type: {
            type:  DataTypes.STRING,
            comment: '来源，1：招工，2：找活，3：跳蚤市场，4：商家'
        },
        date: {
            type:  DataTypes.STRING,
            comment: '拨打时间',
            get() {
                return moment(this.getDataValue('create_time')).format('YYYY-MM-DD HH:mm:ss');
            }
        }
    }),
    /**
     * 添加联系记录
     */
    async insertTelPhone(query) {
        const {content, userId, contentId, type} = query;
        let telPhoneInfo = await this.getPhoneFromUserIdContentId(userId, contentId);
        console.log(telPhoneInfo)
        if (telPhoneInfo[0]) {
            // 更新拨打时间
            this.telPhone.update(
                {
                    date: timeFormat()
                },
                {
                    where: {
                        userId,
                        contentId
                    }
                })
                .then(result => {
                    console.log('更新联系记录成功');
                })
                .catch(err => {
                    console.log('更新联系记录失败');
                })
        }
        else {
            // 新增
            let obj = {
                content,
                userId,
                contentId,
                type,
                date: timeFormat()
            };
            this.telPhone.create(obj)
                .then(result => {
                    console.log('添加联系记录成功')
                })
                .catch(err => {
                    console.log('添加联系记录失败insertTelPhone:', err);
                });
            }
    },
    // 根据用户id和contentid查找拨打记录
    getPhoneFromUserIdContentId(userId, contentId) {
        console.log('userId, contentId', userId, contentId);
        return this.telPhone.findAll({
            where: {
                userId,
                contentId
            }
        })
        .then(result => {
            return result;
        })
        .catch(err => {
            return [];
        })
    },
    /**
     * 获取联系列表
     */
    getTelPhoneList(req, res, next) {
        let {page, pageSize = 15, userId} = req.query;
        let limitStar = page * pageSize - pageSize;
        this.telPhone.findAll({
            where: {
                userId
            },
            order: [['date', 'desc']],
            offset: limitStar || 0,
            limit: +pageSize,
            raw: true
        })
            .then(result => {
                res.send({
                    code: 200,
                    msg: '操作成功',
                    data: result
                });
            })
            .catch(err => {
                console.log('获取联系列表失败getConllectionList：', err);
                res.send({
                    code: 400,
                    msg: '操作失败',
                    data: err
                });
            });
    },

    /**
     * 获取联系记录详情
     */
    getTelPhoneDetai(req, res, next) {
        let {id, userId, contentId, type} = req.query;
        let where = {};
        if (id) {
            where = {
                id
            };
        }
        else {
            where = {
                userId,
                contentId,
                type
            };
        }
        this.telPhone.findAll({
            where: {
                ...where
            }
        })
            .then(result => {
                res.send({
                    code: 200,
                    msg: '操作成功',
                    data: result
                });
            })
            .catch(err => {
                console.log('获取联系记录详情失败getTelPhoneDetai:', err);
                res.send({
                    code: 400,
                    msg: '操作失败',
                    data: err
                });
            });
    },

    /**
     * 联系总数
     */
    telPhoneCount(userId) {
        return this.telPhone.count({
            where: {
                userId
            }
        })
            .then(result => {
                return result;
            })
            .catch(err => {
                console.log('获取收藏总数失败telPhoneCount', err);
            })
    }
};
