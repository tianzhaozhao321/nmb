/**
 * 活动模型
 */

const {defineModel, sequelize} = require('../db');
const {DataTypes, Op } = require("sequelize");
const {timeString} = require('../util/util.js');
const moment = require('moment');


module.exports = {
    activity: defineModel('activity', {
        title: {
            type: DataTypes.STRING,
            comment: '活动标题'
        },
        describe: {
            type: DataTypes.TEXT,
            comment: '活动描述'
        },
        date: {
            type:  DataTypes.STRING,
            comment: '发布时间',
            get() {
                return moment(this.getDataValue('create_time')).format('YYYY-MM-DD HH:mm:ss');
            }
        },
        expirationTime: {
            type:  DataTypes.STRING,
            comment: '过期时间',
            get() {
                return moment(this.getDataValue('create_time')).format('YYYY-MM-DD HH:mm:ss');
            }
        },
        contentId: {
            type: DataTypes.STRING,
            comment: '活动资源ID'
        },
        type: {
            type: DataTypes.INTEGER,
            comment: '数据类型：1：招工，2：找活，3：跳蚤市场，4：商家'
        },
        images: {
            type: DataTypes.TEXT,
            allowNull: true,
            comment: '图片'
        },
        userId: {
            type:  DataTypes.STRING,
            comment: '创建活动的用户id'
        },
        receive: {
            type: DataTypes.INTEGER,
            comment: '活动被领取的次数',
            defaultValue: 0,
            allowNull: true
        },
        useNumber: {
            type: DataTypes.INTEGER,
            comment: '成功兑换个数',
            defaultValue: 0,
            allowNull: true
        },
        latitude: {
            type:  DataTypes.STRING,
            comment: '纬度'
        },
        longitude: {
            type:  DataTypes.STRING,
            comment: '精度'
        },
    }),
    /**
     * 新增活动
     */
    insertActivity(req, res, next) {
        let query = req.body;
        const {id, title, describe, contentId, type, images, userId, latitude, longitude} = query;
        // 添加测试数据 ?name=田&gender=1&birthday=2022-04-07&type=技工&phone=phone&introduce=第一份工作&userId=1&adcode=110108&formattedAddress=北京市海淀区长春桥路17号-海淀区人民政府内&latitude=39.95933&longitude=116.29845
        let obj = {
            title,
            describe,
            contentId,
            type,
            images,
            userId,
            latitude,
            longitude,
            date: timeString(),
            expirationTime: timeString() + 30 * 24 * 60 * 60 * 1000
        }
        if (id) {
            obj.id = id;
            // 更新
            this.activity.update(
                obj,
                {
                    where: {
                        id
                    }
                })
                .then(result => {
                    res.send({
                        code: 200,
                        msg: '操作成功',
                        data: {
                            id
                        }
                    })
                })
                .catch(err => {
                    console.log('更新失败insertActivity：', err);
                })
        }
        else {
            // 新增
            this.activity.create(obj)
            .then(result=> {
                let shop = require('./shop');
                shop.shop.update({
                    activityId: result.dataValues.id
                },
                {
                    where: {
                        id: contentId
                    }
                })
                res.send({
                    code: 200,
                    msg: '操作成功',
                    data: {
                        id: result.dataValues.id
                    }
                });
                console.log('插入成功', res);
            })
            .catch(err => {
                res.json({
                    code: 400,
                    msg: '新增失败'
                });
                console.log('插入失败', err);
            });
        }
    },
    /**
     * 根据ID查询活动详情
     */
    getActivity(req, res, next) {
        let query = req.query;
        const {id, userId} = query;
        this.activity.findAll({
            where: {
                id,
                expirationTime: {
                    [Op.gt]: timeString()
                },
            },
            raw: true
        })
            .then(result => {
                let activityUser = require('./activityUser');
                activityUser.activityUser.findAll({
                    where: {
                        activityId: id,
                        userId
                    },
                    raw: true
                })
                .then(result1 => {
                    // 0 未使用，1已使用，2未领取
                    result[0].use = 2;
                    if (result1.length) {
                        result[0].use = result1[0].use;
                        result[0].activityId = result1[0].id;
                        if (result1[0].use == 0) {
                            let qrSvg = require('./qr-image.js');
                            let svgStr = qrSvg.qrSvg(`?id=${result1[0].id}`);
                            result[0].svg = svgStr;
                        }
                    }
                    res.send({
                        code: 200,
                        msg: '操作成功',
                        data: result
                    });
                })
                .catch(err => {
                    console.log('根据ID查询活动详情getActivity：', err);
                    res.send({
                        code: 400,
                        msg: '操作失败',
                        data: err
                    });
                })
                
            })
            .catch(err => {
                console.log('根据ID查询活动详情getActivity：', err);
                res.send({
                    code: 400,
                    msg: '操作失败',
                    data: err
                });
            });
    },
    /**
     * 根据userId查询活动
     */
     getActivityUser(req, res, next) {
        let query = req.query;
        const {userId} = query;
        this.activity.findAll({
            where: {
                userId,
                expirationTime: {
                    [Op.gt]: timeString()
                }
            }
        })
            .then(result => {
                res.send({
                    code: 200,
                    msg: '操作成功',
                    data: result
                });
            })
            .catch(err => {
                res.json({
                    code: 400,
                    msg: '操作失败',
                    data: err
                });
                console.log('根据用户ID获取简历失败getFindJobUser:', err);
            });
    },

    /**
     * 根据contentId查询活动
     */
    getActivityContentId(req, res, next) {
        let query = req.query;
        const {contentId} = query;
        this.activity.findAll({
            where: {
                contentId,
                expirationTime: {
                    [Op.gt]: timeString()
                }
            }
        })
            .then(result => {
                res.send({
                    code: 200,
                    msg: '操作成功',
                    data: result
                });
            })
            .catch(err => {
                res.json({
                    code: 400,
                    msg: '操作失败',
                    data: err
                });
                console.log('根据用户ID获取简历失败getFindJobUser:', err);
            });
    },
    getActivityJuli(req, res, next) {
        let query = req.query;
        const {lat, long, userId} = query;
        let sql = 
        `select * from (
            (SELECT
                *,
                ROUND(
                    6378.138 * 2 * ASIN(
                        SQRT(
                            POW(
                                SIN((
                                        ${lat} * PI()/ 180-latitude * PI()/ 180 
                                        )/ 2 
                                ),
                                2 
                                )+ COS( ${lat} * PI()/ 180 )* COS( latitude * PI()/ 180 )* POW(
                                SIN((
                                        ${long} * PI()/ 180-longitude * PI()/ 180 
                                        )/ 2 
                                ),
                                2 
                            )))* 1000 
                ) AS distance 
            FROM
                activity) as s
        )
        where
            distance < 20000
        ORDER BY
            distance ASC`
        sequelize.query(sql, {type: sequelize.QueryTypes.SELECT})
        .then(async result => {
            if (userId && result.length) {
                let contentId = [];
                result.forEach(val => {
                    contentId.push(val.id);
                });
                let userActitity = await this.userActivity(userId, contentId);
                if (userActitity.length) {
                    result.forEach((val, index) => {
                        let status = false;
                        userActitity.forEach(newVal => {
                            if (newVal.activityId == val.id) {
                                status = true;
                            }
                        });
                        if(status) {
                            result.splice(index, 1);
                        }
                    });
                }
            }
            res.send({
                code: 200,
                msg: '操作成功',
                data: result
            });
            
        }).catch(err => {
            res.send({
                code: 400,
                msg: '操作失败',
                data: ''
            });
            console.log('获取全部数据失败getAllList：', err);
        });
    },
    // 判断用户是否已领取活动
    userActivity(userId, contentId) {
        let activityUser = require('./activityUser');
        return activityUser.activityUser.findAll({
            where: {
                userId,
                activityId: {
                    [Op.in]: contentId
                }
            },
            raw: true
        })
            .then(res => {
                return res;
            })
            .catch(err => {
                console.log('判断用户是否已领取活动失败userActivity', err);
                return Promise.reject();
            })
    }
}