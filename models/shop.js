/**
 * 店铺模型
 */
//  DROP TABLE IF EXISTS `shop`;
//  CREATE TABLE `shop` (
//    `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
//    `showName` varchar(100) NOT NULL COMMENT '名称',
//    `shopDescribe` varchar(255) NOT NULL COMMENT '描述',
//    `formattedAddress` varchar(255) NOT NULL COMMENT '定位地址',
//    `detailAddr` varchar(255) NOT NULL COMMENT '详细地址',
//    `latitude` varchar(40) NOT NULL COMMENT '维度',
//    `longitude` varchar(40) NOT NULL COMMENT '精度',
//    `adcode` varchar(10) NOT NULL COMMENT '县级编码',
//    `shopType` varchar(10) NOT NULL COMMENT '店铺类型',
//    `date` varchar(20) NOT NULL COMMENT '新增或编辑日期',
//    `images` text NOT NULL COMMENT '图片列表',
//    `examineStatus` int(2) NOT NULL COMMENT '审核状态，1待审核，2审核不通过，3审核通过',
//    `examine` varchar(255) DEFAULT NULL COMMENT '审核失败描述',
//    `userId` int(11) NOT NULL COMMENT '用户id',
//    `phone` varchar(30) NOT NULL COMMENT '联系电话',
//    `cityCode` varchar(20) DEFAULT NULL COMMENT '市',
//    `provinceCode` varchar(20) DEFAULT NULL COMMENT '省',
//    PRIMARY KEY (`id`) USING BTREE
//  ) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
const {defineModel, sequelize} = require('../db');
const {DataTypes} = require('sequelize');
const {getProvinceCityCode} = require('../util/apiUtil.js');
const {timeString, customWhere, formatTime} = require('../util/util.js');
const moment = require('moment');
const user = require('./user.js');
module.exports = {
    shop: defineModel('shop', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        shopDescribe: {
            type: DataTypes.TEXT,
            comment: '描述'
        },
        userId: {
            type: DataTypes.INTEGER,
            comment: '用户ID'
        },
        showName: {
            type: DataTypes.STRING,
            comment: '名称'
        },
        shopType: {
            type:  DataTypes.STRING,
            comment: '店铺类型'
        },
        adcode: {
            type:  DataTypes.STRING,
            comment: '县级编码'
        },
        latitude: {
            type:  DataTypes.STRING,
            comment: '纬度'
        },
        longitude: {
            type:  DataTypes.STRING,
            comment: '精度'
        },
        phone: {
            type:  DataTypes.STRING,
            comment: '联系电话'
        },
        date: {
            type:  DataTypes.STRING,
            comment: '发布时间',
            get() {
                return moment(+this.getDataValue('date')).format('MM-DD HH:mm');
            }
        },
        images: {
            type: DataTypes.TEXT,
            allowNull: true,
            comment: '图片'
        },
        formattedAddress: {
            type:  DataTypes.STRING,
            comment: '格式化后的地址'
        },
        detailAddr: {
            type:  DataTypes.STRING,
            comment: '详细地址',
            allowNull: true,
        },
        cityCode: {
            type:  DataTypes.STRING,
            comment: '城市编码'
        },
        provinceCode: {
            type:  DataTypes.STRING,
            comment: '省编码'
        },
        examineStatus: {
            type: DataTypes.INTEGER(2),
            comment: '审核状态，1待审核，2审核不通过，3审核通过'
        },
        activityId: {
            type: DataTypes.INTEGER(10),
            allowNull: true,
            content: '活动ID'
        }
    }),
    async insertShop(req, res, next) {
        let query = req.body;
        const {id,
            adcode,
            formattedAddress,
            latitude,
            longitude,
            showName,
            shopDescribe,
            detailAddr,
            shopType,
            images,
            userId,
            phone
        } = query;

        try {
            let pcValue = await getProvinceCityCode(adcode);
            let obj = {
                showName,
                shopDescribe,
                formattedAddress,
                detailAddr,
                latitude,
                longitude,
                adcode,
                shopType,
                images,
                userId,
                phone,
                cityCode: pcValue.cityCode,
                provinceCode: pcValue.provinceCode,
                date: timeString(),
                examineStatus: 1
            }
            if (id) {
                obj.id = id;
                // 更新
                this.shop.update(
                    obj,
                    {
                        where: {
                            id
                        }
                    })
                    .then(result => {
                        res.send({
                            code: 200,
                            msg: '操作成功',
                            data: {}
                        })
                    })
                    .catch(err => {
                        console.log('更新失败insertShop：', err);
                    })
            }
            else {
                // 新增
                this.shop.create(obj)
                .then(result=> {
                    user.updataUserInfo(result.dataValues.id, userId, 'shop')
                        .then(result1 => {
                            res.send({
                                code: 200,
                                msg: '操作成功',
                                data: {
                                    id: result.dataValues.id
                                }
                            })
                            console.log('插入成功', res);
                        })
                        .catch(err1 => {
                            console.log('插入失败:insertShop', err1);
                            res.json({
                                code: 400,
                                msg: '新增失败'
                            });
                        });
                })
                .catch(err => {
                    res.json({
                        code: 400,
                        msg: '新增失败'
                    });
                    console.log('插入失败', err);
                });
            }
        }
        catch(err) {
            console.log('获取省市编码错误insertShop：', err);
            res.send({
                code: 400,
                msg: '获取省市编码错误',
                data: {}
            });
        };
    },
    /**
     * 获取店铺详情
     */
    getShop(req, res, next) {
        let query = req.query;
        const {id} = query;
        this.shop.findAll({
            where: {
                id
            }
        })
            .then(result => {
                res.send({
                    code: 200,
                    msg: '操作成功',
                    data: result
                });
            })
            .catch(err => {
                res.send({
                    code: 400,
                    msg: '操作失败',
                    data: err
                });
            });
    },
    /**
     * 根据用户id获取店铺列表
     */ 
    getShopList(req, res, next) {
        let query = req.query;
        const {userId} = query;
        this.shop.findAll({
            where: {
                userId
            },
            order: [['date', 'desc']],
        })
            .then(result => {
                res.send({
                    code: 200,
                    msg: '操作成功',
                    data: result
                });
            })
            .catch(err => {
                console.log('获取用户的店铺列表失败getShopList：', err);
                res.send({
                    code: 400,
                    msg: '操作失败',
                    data: err
                });
            });
    },
    /**
     * 获取店铺列表
     **/
    getShopListAll(req, res, next) {
        let {page, pageSize=15, type} = req.query;
        let limitStar = page * pageSize - pageSize;
        let where = customWhere(req.query);
        if (type && type !== '全部') {
            where.shopType = type;
        }
        console.log('getShowlistAll', where);
        this.shop.findAll({
            attributes: [
                'id', ['showName', 'title'], ['shopType', 'type'], 'formattedAddress', 'phone', 'date'
            ],
            where: {
                examineStatus: 3,
                ...where
            },
            order: [['date', 'desc']],
            offset: limitStar || 0,
            limit: +pageSize,
            raw: true
        })
            .then(result => {
                res.json({
                    code: 200,
                    msg: '操作成功',
                    data: result.map(val => {
                        val.busnessType = 4;
                        return val;
                    })
                });
            })
            .catch(err => {
                console.log('获取店铺列表失败getShopListAll', err);
                res.json({
                    code: 400,
                    msg: '操作失败',
                    data: err
                });
            });
    },
    // 更新店铺状态
    updateExamineStatus(id, status) {
        return this.shop.update({
            examineStatus: status
        },
        {
            where: {
                id
            }
        })
            .then(res => {
                return res;
            })
            .catch(err => {
                console.log('更新店铺失败updateExamineStatus', err)
                return Promise.reject(err);
            })
    }
};