/**
 * 跳蚤市场模型
 */
//  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
//  `userId` int(20) NOT NULL COMMENT '用户ID',
//  `userName` varchar(100) NOT NULL COMMENT '联系人',
//  `phone` varchar(100) NOT NULL COMMENT '联系电话',
//  `title` varchar(100) NOT NULL COMMENT '标题',
//  `type` varchar(255) NOT NULL COMMENT '类型',
//  `marketDescribe` text NOT NULL COMMENT '商品描述',
//  `images` text NOT NULL COMMENT '图片',
//  `date` varchar(20) NOT NULL COMMENT '新增或编辑日期',
//  `adcode` varchar(10) NOT NULL COMMENT '县级编码',
//  `latitude` varchar(40) NOT NULL COMMENT '维度',
//  `longitude` varchar(40) NOT NULL COMMENT '精度',
//  `formattedAddress` varchar(255) NOT NULL COMMENT '定位地址',
//  `examineStatus` int(2) NOT NULL COMMENT '审核状态，1待审核，2审核不通过，3审核通过',
//  `cityCode` varchar(20) NOT NULL COMMENT '市',
//  `provinceCode` varchar(20) NOT NULL COMMENT '省',
const {defineModel, sequelize} = require('../db');
const { Op } = require("sequelize");
const {DataTypes} = require('sequelize');
const {getProvinceCityCode} = require('../util/apiUtil.js');
const {timeString, customWhere} = require('../util/util.js');
const moment = require('moment');
const user = require('./user.js');

module.exports = {
    market: defineModel('market', {
        marketDescribe: {
            type: DataTypes.TEXT,
            comment: '自我介绍'
        },
        userId: {
            type: DataTypes.INTEGER,
            comment: '用户ID'
        },
        title: {
            type: DataTypes.STRING,
            comment: '标题'
        },
        userName: {
            type: DataTypes.STRING,
            comment: '姓名'
        },
        type: {
            type: DataTypes.STRING,
            comment: '期望工种'
        },
        adcode: {
            type: DataTypes.STRING,
            comment: '县级编码'
        },
        latitude: {
            type: DataTypes.STRING,
            comment: '纬度'
        },
        longitude: {
            type: DataTypes.STRING,
            comment: '精度'
        },
        phone: {
            type: DataTypes.STRING,
            comment: '联系电话'
        },
        date: {
            type: DataTypes.STRING,
            comment: '发布时间',
            get() {
                return moment(+this.getDataValue('date')).format('MM-DD HH:mm');
            }
        },
        examineStatus: {
            type: DataTypes.INTEGER(2),
            comment: '审核状态，1待审核，2审核不通过，3审核通过'
        },
        images: {
            type: DataTypes.TEXT,
            allowNull: true,
            comment: '图片'
        },
        formattedAddress: {
            type: DataTypes.STRING,
            comment: '格式化后的地址'
        },
        cityCode: {
            type: DataTypes.STRING,
            comment: '城市编码'
        },
        provinceCode: {
            type: DataTypes.STRING,
            comment: '省编码'
        }
    }),
    async insertMarket(req, res, next) {
        let query = req.body;
        const {
            id,
            userName,
            phone,
            title,
            type,
            marketDescribe,
            images,
            userId,
            adcode,
            latitude,
            longitude,
            formattedAddress
        } = query;
        if (!userId) {
            res.json({
                code: 400,
                msg: '用户ID不存在',
                data: {}
            });
        }
        // 添加测试数据 ?name=田&gender=1&birthday=2022-04-07&type=技工&phone=phone&introduce=第一份工作&userId=1&adcode=110108&formattedAddress=北京市海淀区长春桥路17号-海淀区人民政府内&latitude=39.95933&longitude=116.29845
        try {
            let pcValue = await getProvinceCityCode(adcode);
            let obj = {
                userName,
                phone,
                title,
                type,
                marketDescribe,
                images,
                userId,
                adcode,
                latitude,
                longitude,
                formattedAddress,
                cityCode: pcValue.cityCode,
                provinceCode: pcValue.provinceCode,
                date: timeString(),
                examineStatus: 1
            };
            if (id) {
                obj.id = id;
                // 更新
                this.market.update(
                    obj,
                    {
                        where: {
                            id
                        }
                    })
                    .then(result => {
                        res.send({
                            code: 200,
                            msg: '操作成功',
                            data: {}
                        });
                    })
                    .catch(err => {
                        console.log('更新失败insertMarket：', err);
                    });
            }
            else {
                // 新增
                this.market.create(obj)
                .then(result=> {
                    user.updataUserInfo(result.dataValues.id, userId, 'market')
                        .then(result1 => {
                            res.send({
                                code: 200,
                                msg: '操作成功',
                                data: {
                                    id: result.dataValues.id
                                }
                            });
                            console.log('插入成功', res);
                        })
                        .catch(err1 => {
                            console.log('插入失败:insertMarket', err1);
                            res.json({
                                code: 400,
                                msg: '新增失败'
                            });
                        });
                })
                .catch(err => {
                    res.json({
                        code: 400,
                        msg: '新增失败'
                    });
                    console.log('插入失败insertMarket', err);
                });
            }
        }
        catch (err) {
            console.log('获取省市编码错误insertMarket：', err);
            res.send({
                code: 400,
                msg: '获取省市编码错误',
                data: {}
            });
        };
    },
    /**
     * 根据ID查询跳蚤详情
     */
    getMarket(req, res, next) {
        let query = req.query;
        const {id} = query;
        this.market.findAll({
            where: {
                id
            }
        })
            .then(result => {
                res.send({
                    code: 200,
                    msg: '操作成功',
                    data: result
                });
            })
            .catch(err => {
                console.log('获取跳蚤市场详情失败getMarket：', err);
                res.sed({
                    code: 400,
                    msg: '操作失败',
                    data: err
                });
            });
        let sql = `select * from market where id='${id}'`;
    },

    /**
      * 根据userId查询跳蚤列表
      */
    getMarketUser(req, res, next) {
        let query = req.query;
        const {userId} = query;
        this.market.findAll({
            where: {
                userId
            },
            order: [['date', 'desc']]
        })
            .then(result => {
                res.send({
                    code: 200,
                    msg: '操作成功',
                    data: result
                });
            })
            .catch(err => {
                console.log('获取用户跳蚤列表失败getMarketUser:', err);
                res.send({
                    code: 400,
                    msg: '操作失败',
                    data: err
                });
            });
    },

    /**
     * 列表页--跳蚤市场
     */
    getMarketList(req, res, next) {
        let {page, pageSize, type} = req.query;
        let limitStar = page * pageSize - pageSize;
        let where = customWhere(req.query);
        if (type && type !== '全部') {
            where.type = type;
        }
        this.market.findAll({
            attributes: ['id', 'title', 'type', 'formattedAddress', 'phone', 'date'],
            where: {
                examineStatus: 3,
                ...where
            },
            order: [['date', 'desc']],
            offset: limitStar || 0,
            limit: +pageSize
        })
            .then(result => {
                res.send({
                    code: 200,
                    msg: '操作成功',
                    data: result
                });
            })
            .catch(err => {
                console.log('获取跳蚤市场列表失败getMarketList：', err);
                res.send({
                    code: 400,
                    msg: '操作失败',
                    data: err
                });
            });
    },
    /**
     * 获取二手列表
     */
    getErShou(req, res, next) {
        let {page, pageSize = 15, type} = req.query;
        let limitStar = page * pageSize - pageSize;
        let where = {
            type: '二手交易',
            ...customWhere(req.query)
        };
        this.market.findAll({
            attributes: ['id', 'title', 'type', 'formattedAddress', 'phone', 'date'],
            where: {
                ...where
            },
            order: [['date', 'desc']],
            offset: limitStar || 0,
            limit: +pageSize,
            raw: true
        })
            .then(result => {
                res.send({
                    code: 200,
                    msg: '操作成功',
                    data: result.map(val => {
                        val.busnessType = 3;
                        return val;
                    })
                });
            })
            .catch(err => {
                console.log('获取跳蚤市场列表失败getMarketList：', err);
                res.send({
                    code: 400,
                    msg: '操作失败',
                    data: err
                });
            });
    },
    /**
     * 首页获取跳蚤市场
     */
    homeGetMarketList(req, res, next) {
        let {page, pageSize = 15, type} = req.query;
        let limitStar = page * pageSize - pageSize;
        let where = {
            type: {
                [Op.ne]: '1007'
            },
            ...customWhere(req.query)
        };
        this.market.findAll({
            attributes: ['id', 'title', 'type', 'formattedAddress', 'phone', 'date'],
            where: {
                ...where
            },
            order: [['date', 'desc']],
            offset: limitStar || 0,
            limit: +pageSize,
            raw: true
        })
            .then(result => {
                res.send({
                    code: 200,
                    msg: '操作成功',
                    data: result.map(val => {
                        val.busnessType = 3;
                        return val;
                    })
                });
            })
            .catch(err => {
                console.log('获取跳蚤市场列表失败getMarketList：', err);
                res.send({
                    code: 400,
                    msg: '操作失败',
                    data: err
                });
            });
    },
    // 更新跳蚤市场审核状态
    updateExamineStatus(id, status) {
        return this.market.update({
            examineStatus: status
        },
        {
            where: {
                id
            }
        })
            .then(res => {
                return res;
            })
            .catch(err => {
                console.log('更新跳蚤市场失败updateExamineStatus', err)
                return Promise.reject(err);
            })
    }
};