/**
 * @file 招工类型
 */
const db = require('../db');
const {DataTypes } = require("sequelize");
module.exports = {
    workType: db.defineModel('workType', {
        name: {
            type: DataTypes.STRING,
            allowNull: true,
            comment: '类型名称'
        },
    }),
    // 店铺类型
    getWorkType(req, res, next) {
        this.workType.findAll()
            .then(result => {
                res.send({
                    code: 200,
                    msg: '操作成功',
                    data: result
                });
            })
            .catch(err => {
                res.json({
                    code: 400,
                    msg: '操作失败',
                    data: err
                })
            });
    },
}