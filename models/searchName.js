/**
 * @file 搜索模型
 */
const {defineModel, sequelize} = require('../db');
const {DataTypes } = require("sequelize");
const {timeFormat} = require('../util/util.js');
module.exports = {
    searchName: defineModel('searchName', {
        name: {
            type: DataTypes.TEXT,
            comment: '搜索关键词'
        },
        userId: {
            type: DataTypes.STRING,
            comment: '用户id'
        },
        date: {
            type:  DataTypes.STRING,
            comment: '搜索时间'
        }
    }),
    // 店铺类型
    async insertsearchName(userId, name) {
        let obj = {
            name,
            userId,
            date: timeFormat()
        };
        console.log(obj)
        this.searchName.create(obj)
        .then(result=> {
            console.log('插入搜索关键词成功');
        })
        .catch(err => {
            console.log('插入搜索关键词失败', err);
        });
    }
};
 