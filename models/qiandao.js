/**
 * 活动模型
 */

 const {defineModel, sequelize} = require('../db');
 const {DataTypes, Op } = require("sequelize");
 const {timeString} = require('../util/util.js');
 const moment = require('moment');
 const user = require('./user.js');
 
 module.exports = {
    qiandao: defineModel('qiandao', {
         num: {
             type: DataTypes.INTEGER,
             comment: '签到计数'
         },
     }),
     getQiandao(req, res, next) {
        this.qiandao.findAll({
            raw: true
        })
            .then(result => {
                res.send({
                    code: 200,
                    msg: '操作成功',
                    data: result[0]
                });
                
            })
            .catch(err => {
                console.log('获取签到数据失败：', err);
                res.send({
                    code: 400,
                    msg: '操作失败',
                    data: err
                });
            });
     },
     insertQiandao(req, res, next) {
        this.qiandao.findAll({
            raw: true
        })
            .then(result => {
                let num = result[0].num + 1;
                this.qiandao.update(
                    {
                        num
                    },
                    {
                        where: {
                            id: 1
                        }
                    })
                    .then(() => {
                        res.send({
                            code: 200,
                            msg: '操作成功',
                            data: {
                                num
                            }
                        })
                    })
                    .catch(err => {
                        console.log('新增签到失败', err);
                    })
            })
            .catch(err => {
                console.log('新增签到失败', err);
                res.send({
                    code: 400,
                    msg: '操作失败',
                    data: err
                });
            });
    }
}