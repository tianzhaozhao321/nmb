const qr = require('qr-image');
module.exports = {
    qrSvg(str) {
        let svg = qr.imageSync(str, {
            type: 'svg'
        });
        var b = new Buffer(svg);
        var s = b.toString('base64');
        return 'data:image/svg+xml;base64,' + s;
    }
};