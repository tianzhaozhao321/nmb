/**
 * 跳蚤市场模型
 */
//  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
//  `userId` int(20) NOT NULL COMMENT '用户ID',
//  `userName` varchar(100) NOT NULL COMMENT '联系人',
//  `phone` varchar(100) NOT NULL COMMENT '联系电话',
//  `title` varchar(100) NOT NULL COMMENT '标题',
//  `type` varchar(255) NOT NULL COMMENT '类型',
//  `marketDescribe` text NOT NULL COMMENT '商品描述',
//  `images` text NOT NULL COMMENT '图片',
//  `date` varchar(20) NOT NULL COMMENT '新增或编辑日期',
//  `adcode` varchar(10) NOT NULL COMMENT '县级编码',
//  `latitude` varchar(40) NOT NULL COMMENT '维度',
//  `longitude` varchar(40) NOT NULL COMMENT '精度',
//  `formattedAddress` varchar(255) NOT NULL COMMENT '定位地址',
//  `examineStatus` int(2) NOT NULL COMMENT '审核状态，1待审核，2审核不通过，3审核通过',
//  `cityCode` varchar(20) NOT NULL COMMENT '市',
//  `provinceCode` varchar(20) NOT NULL COMMENT '省',
const {defineModel, sequelize} = require('../db');
const {DataTypes} = require('sequelize');

module.exports = {
    hot: defineModel('hot', {
        status: {
            type: DataTypes.BOOLEAN,
            default: false,
            comment: '状态'
        }
    }),
    /**
     * 根据ID查询跳蚤详情
     */
    getStatus(req, res, next) {
        let query = req.query;
        this.hot.findAll()
            .then(result => {
                res.send({
                    code: 200,
                    msg: '操作成功',
                    data: result[0]
                });
            })
            .catch(err => {
                console.log('获取hot状态失败getStatus', err);
                res.sed({
                    code: 400,
                    msg: '操作失败',
                    data: err
                });
            });
    }
};
