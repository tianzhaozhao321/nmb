/**
 * @file 招工模型
 */
const {defineModel, sequelize} = require('../db');
const {DataTypes } = require("sequelize");
const {getProvinceCityCode} = require('../util/apiUtil.js');
const {timeString, timeFormat, timeFormatM, timeFormatPreData} = require('../util/util.js');
const moment = require('moment');
const user = require('./user.js');

// DROP TABLE IF EXISTS `recruitment`;
// CREATE TABLE `recruitment` (
//   `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
//   `workDescribe` text NOT NULL COMMENT '招工详情',
//   `contacts` varchar(100) NOT NULL COMMENT '联系人',
//   `phone` varchar(30) NOT NULL COMMENT '手机号',
//   `formattedAddress` varchar(255) NOT NULL COMMENT '定位地址',
//   `latitude` varchar(40) NOT NULL COMMENT '维度',
//   `longitude` varchar(40) NOT NULL COMMENT '精度',
//   `adcode` varchar(10) NOT NULL COMMENT '县级编码',
//   `type` varchar(255) NOT NULL COMMENT '店铺类型',
//   `date` varchar(20) NOT NULL COMMENT '新增或编辑日期',
//   `images` text NOT NULL COMMENT '图片列表',
//   `examineStatus` int(2) NOT NULL COMMENT '审核状态，1待审核，2审核不通过，3审核通过',
//   `userId` int(11) NOT NULL COMMENT '用户id',
//   `cityCode` varchar(20) NOT NULL COMMENT '市',
//   `provinceCode` varchar(20) NOT NULL COMMENT '省',
//   PRIMARY KEY (`id`)
// ) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COMMENT='招工表';

module.exports = {
    recruitment: defineModel('recruitment', {
        workDescribe: {
            type: DataTypes.TEXT,
            comment: '招工描述'
        },
        contacts: {
            type: DataTypes.STRING,
            comment: '联系人'
        },
        phone: {
            type: DataTypes.STRING,
            comment: '联系电话'
        },
        formattedAddress: {
            type:  DataTypes.STRING,
            comment: '格式化后的地址'
        },
        latitude: {
            type:  DataTypes.STRING,
            allowNull: true,
            comment: '纬度'
        },
        longitude: {
            type:  DataTypes.STRING,
            allowNull: true,
            comment: '精度'
        },
        adcode: {
            type:  DataTypes.STRING,
            comment: '县级编码'
        },
        type: {
            type:  DataTypes.STRING,
            comment: '招工类型'
        },
        date: {
            type:  DataTypes.STRING,
            comment: '发布时间'
        },
        images: {
            type: DataTypes.TEXT,
            allowNull: true,
            comment: '图片'
        },
        examineStatus: {
            type: DataTypes.INTEGER(2),
            comment: '审核状态，1待审核，2审核不通过，3审核通过'
        },
        userId: {
            type:  DataTypes.STRING,
            comment: '用户id'
        },
        toping: {
            type: DataTypes.STRING,
            allowNull: true,
            comment: '置顶，有日期的置顶，没日期的不置顶'
        },
        cityCode: {
            type:  DataTypes.STRING,
            comment: '城市编码'
        },
        provinceCode: {
            type:  DataTypes.STRING,
            comment: '省编码'
        },
        closed: {
            type: DataTypes.STRING,
            allowNull: true,
            comment: '关闭，1未关闭，2已关闭'
        },
        browseNum: {
            type: DataTypes.INTEGER,
            defaultValue: 0,
            comment: '浏览次数'
        },
        tags: {
            type: DataTypes.TEXT,
            allowNull: true,
            comment: '标签'
        }
    }),
    // 店铺类型
    async insertRecruitment(req, res, next) {
        let query = req.body;
        const {
            id,
            workDescribe,
            adcode,
            formattedAddress,
            latitude,
            longitude,
            shopDescribe,
            type,
            images,
            contacts,
            phone,
            userId} = query;
        if (!userId) {
            res.send({
                code: 400,
                msg: '用户ID不存在',
                data: {}
            });
        }
        try {
            // 插入测试数据 http://localhost/addRecruitment?adcode=110114&formattedAddress=%E5%8C%97%E4%BA%AC%E5%B8%82%E6%98%8C%E5%B9%B3%E5%8C%BA%E6%94%BF%E5%BA%9C%E8%A1%9719%E5%8F%B7-%E6%98%8C%E5%B9%B3%E5%8C%BA%E6%94%BF%E5%BA%9C%E5%86%85&latitude=40.22077&longitude=116.23128&workDescribe=aaaa&contacts=tian&phone=15810525430&userId=1&images=http://tianzhaozhao.bcc-bdbl.baidu.com:8000/static/img/dbaf98befd9d6150b3aca7a6b9d682c4.png&type=%E6%8A%80%E5%B7%A5
            // 更新测试数据 http://localhost/addRecruitment?id=2&adcode=110114&formattedAddress=%E5%8C%97%E4%BA%AC%E5%B8%82%E6%98%8C%E5%B9%B3%E5%8C%BA%E6%94%BF%E5%BA%9C%E8%A1%9719%E5%8F%B7-%E6%98%8C%E5%B9%B3%E5%8C%BA%E6%94%BF%E5%BA%9C%E5%86%85&latitude=40.22077&longitude=116.23128&workDescribe=aaaa&contacts=tian&phone=15810525430&userId=2&images=http://tianzhaozhao.bcc-bdbl.baidu.com:8000/static/img/dbaf98befd9d6150b3aca7a6b9d682c4.png&type=%E6%8A%80%E5%B7%A5
            let pcValue = await getProvinceCityCode(adcode);
            console.log(pcValue);
            let obj = {
                workDescribe,
                formattedAddress,
                latitude,
                longitude,
                adcode,
                type,
                images,
                contacts,
                phone,
                userId,
                cityCode: pcValue.cityCode,
                provinceCode: pcValue.provinceCode,
                examineStatus: 1,
                toping: 1,
                closed: 1
            };
            if (id) {
                obj.id = id;
                // 更新
                this.recruitment.update(
                    obj,
                    {
                        where: {
                            id
                        }
                    })
                    .then(result => {
                        res.send({
                            code: 200,
                            msg: '操作成功',
                            data: {}
                        })
                    })
                    .catch(err => {
                        console.log('更新失败insertRecruitment:', err);
                    })
            }
            else {
                // 新增
                obj.date = timeFormatM();
                this.recruitment.create(obj)
                .then(result=> {
                    user.updataUserInfo(result.dataValues.id, userId, 'recruitment')
                        .then(result1 => {
                            res.send({
                                code: 200,
                                msg: '操作成功',
                                data: {
                                    id: result.dataValues.id
                                }
                            });
                            console.log('插入成功', res);
                        })
                        .catch(err1 => {
                            console.log('插入失败:insertRecruitment', err1);
                            res.send({
                                code: 400,
                                msg: '新增失败'
                            });
                        })
                })
                .catch(err => {
                    res.send({
                        code: 400,
                        msg: '新增失败'
                    });
                    console.log('插入失败insertRecruitment', err);
                });
            }
        }
        catch(err) {
            console.log('获取省市编码错误:insertRecruitment', err);
            res.send({
                code: 400,
                msg: '获取省市编码错误',
                data: {}
            });
        };
    },
    // 晋州
    daorushuju(req, res, next) {
        let query = req.body;
        const {
            id,
            workDescribe,
            adcode,
            formattedAddress,
            latitude,
            longitude,
            shopDescribe,
            type,
            images,
            contacts,
            phone,
            userId,
            tags} = query;
            this.recruitment.findAll({
                where: {
                    phone
                }
            })
            .then(async result => {
                if (result.length < 1) {
                    let pcValue = await getProvinceCityCode(adcode);
                    let obj = {
                        workDescribe,
                        formattedAddress,
                        latitude,
                        longitude,
                        adcode,
                        type,
                        tags,
                        images,
                        contacts,
                        phone,
                        userId,
                        cityCode: pcValue.cityCode,
                        provinceCode: pcValue.provinceCode,
                        date: timeFormatM(),
                        examineStatus: 3,
                        toping: 1,
                        closed: 1
                    };
                    // 新增
                    this.recruitment.create(obj)
                    .then(result=> {
                        res.send({
                            code: 200,
                            msg: '新增成功'
                        });
                    })
                    .catch(err => {
                        console.log('导入失败', err);
                        res.send({
                            code: 200,
                            msg: '新增失败'
                        });
                    });
                }
                else {
                    res.send({
                        code: 300,
                        msg: '已存在'
                    });
                }
            })
    },
    /**
     * 根据id获取招工详情
     * @param {*} req 
     * @param {*} res 
     * @param {*} next 
     */
    getRecruitment(req, res, next) {
        let query = req.query;
        const {id} = query;
        this.recruitment.findAll({
            where: {
                id
            }
        })
        .then(result => {
            this.updatebrowseNum(result[0].id, result[0].browseNum);
            res.send({
                code: 200,
                msg: '操作成功',
                data: result
            });
        })
        .catch(err => {
            console.log('获取招工详情失败：getRecruitment：', err);
            res.send({
                code: 400,
                msg: '操作失败',
                data: err
            });
        });
    },
    /**
     * 用户查看以后更新浏览次数
     */
    updatebrowseNum(id, browseNum) {
        return this.recruitment.update({
            browseNum: +browseNum + 1
        },
        {
            where: {
                id
            }
        })
            .then(res => {
                console.log('更新浏览次数成功', res)
            })
            .catch(err => {
                console.log('更新浏览次数失败', err)
            })
    },
    /**
     * 根据userId获取工作列表
     */
    getRecruitmentList(req, res, next) {
        let query = req.query;
        const {userId} = query;
        this.recruitment.findAll({
            where: {
                userId
            },
            order: [
                ['date', 'desc']
            ]
        })
        .then(result => {
            res.send({
                code: 200,
                msg: '操作成功',
                data: result
            });
        })
        .catch(err => {
            console.log('根据uid获取招工列表失败getRecruitmentList：', err);
            res.send({
                code: 400,
                msg: '操作失败',
                data: err
            });
        });
    },
    /**
     * 审核列表
     */
    async getRecruitmentListShenhe(req, res) {
        let query = req.query;
        const {userId} = query;
        let userInfo = await user.getUserInfo(userId);
        this.recruitment.findAll({
            where: {
                userId
            },
            order: [
                ['date', 'desc']
            ]
        })
        .then(result => {
            res.send({
                code: 200,
                msg: '操作成功',
                data: {
                    list: result,
                    userInfo
                }
            });
        })
        .catch(err => {
            console.log('根据uid获取招工列表失败getRecruitmentList：', err);
            res.send({
                code: 400,
                msg: '操作失败',
                data: err
            });
        });
    },
    /**
     * 获取全部招工列表
     */
    getRecruitmentListAll(req, res, next) {
        let {page, pageSize, type} = req.query;
        let limitStar = page * pageSize - pageSize;
        // 暂时把地区筛选选项去掉
        // let where = customWhere(req.query);
        // if (type && type !== '全部') {
        //     where.type = type;
        // }
        this.recruitment.findAll({
            attributes: [
                'id', ['workDescribe', 'title'], 'type', 'formattedAddress', 'phone', 'date'
            ],
            where: {
                examineStatus: 3,
                type
            },
            order: [['date', 'desc']],
            offset: limitStar || 0,
            limit: +pageSize,
            raw: true
        })
        .then(result => {
            res.send({
                code: 200,
                msg: '操作成功',
                data: result.map((val) => {
                    val.busnessType = 1;
                    return val;
                })
            });
        })
        .catch(err => {
            res.send({
                code: 400,
                msg: '操作失败',
                data: ''
            });
            console.log('查询失败', err);
        });
    },
    // 审核结果-更新招工状态
    updateExamineStatus(id, status, type, tags) {
        return this.recruitment.update({
            examineStatus: status,
            type,
            tags
        },
        {
            where: {
                id
            }
        })
            .then(res => {
                return res;
            })
            .catch(err => {
                console.log('更新招工失败updateExamineStatus', err)
                return Promise.reject(err);
            })
    },
    // 置顶
    async goToping(req, res) {
        let {id, userId} = req.query;
        let userInfo = await user.getUserInfo(userId);
        if (userInfo[0].integral < 10) {
            res.send({
                code: 300,
                msg: '积分不足',
                data: ''
            });
        }
        else {
            // 减少用户积分
            user.reduceIntegral(userId, userInfo[0].integral - 10);
            // 增加一下用户积分-1的逻辑
            this.recruitment.update({
                toping: timeFormat()
            },
            {
                where: {
                    id
                }
            })
            .then(result => {
                res.send({
                    code: 200,
                    msg: '操作成功',
                    data: result
                });
            })
            .catch(err => {
                res.send({
                    code: 400,
                    msg: '操作失败',
                    data: result
                });
                console.log('置顶操作失败goToping:', err);
            })
        }
    },
    // 到期取消置顶
    cancelZhiding() {
        let sqlStr = `select * from recruitment WHERE toping<'${timeFormatPreData()}' and toping>1`;
        sequelize.query(sqlStr, {type: sequelize.QueryTypes.SELECT})
        .then(result => {
            if (result.length > 0) {
                let id = [];
                for (let i = 0; i < result.length; i++) {
                    id.push(result[i].id);
                }
                let ids = id.join(',');
                let sql = `update recruitment set toping='1' where id in (${ids})`;
                    console.log('sql', sql);
                    sequelize.query(sql, {type: sequelize.QueryTypes.SELECT})
                    .then(res => {
                        console.log('到期取消置顶更新成功')
                    })
                    .catch(err => {
                        console.log('到期取消置顶更新失败', err);
                    })
            }
        })
        .catch(err => {
            console.log('取消置顶失败', err);
        })
    },
    // 刷新
    async refresh(req, res) {
        let {id, userId} = req.query;
        let userInfo = await user.getUserInfo(userId);
        if (userInfo[0].integral < 1) {
            res.send({
                code: 300,
                msg: '积分不足',
                data: ''
            });
        }
        else {
            // 减少用户积分
            user.reduceIntegral(userId, userInfo[0].integral - 1);
            this.recruitment.update({
                date: timeFormat()
            },
            {
                where: {
                    id
                }
            })
            .then(result => {
                res.send({
                    code: 200,
                    msg: '操作成功',
                    data: result
                });
            })
            .catch(err => {
                res.send({
                    code: 400,
                    msg: '操作失败',
                    data: result
                });
                console.log('置顶操作失败goToping:', err);
            })
        }
    },
    /**
     * 更新关闭和打开状态，1打开，2关闭
     */
    closed(req, res) {
        let {id, closed} = req.query;
        this.recruitment.update({
            closed,
            toping: 1
        },
        {
            where: {
                id
            }
        })
        .then(result => {
            res.send({
                code: 200,
                msg: '操作成功',
                data: result
            });
        })
        .catch(err => {
            res.send({
                code: 400,
                msg: '操作失败',
                data: result
            });
            console.log('更新关闭操作失败:', err);
        })
    },
    updateriqi() {
        // timeFormatM
        this.recruitment.findAll({raw: true})
        .then(res => {
            console.log(res[0]);
            let len = res.length;
            for (let i = 0; i< len; i++) {
                let {id, date} = res[i];
                this.recruitment.update(
                    {
                        date: timeFormatM(+date)
                    },
                    {
                        where: {
                            id
                        }
                    }
                )
            }
        })
    }
};
