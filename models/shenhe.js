/**
 * 审核模型
 */
const {defineModel, sequelize} = require('../db');
const {DataTypes, where } = require("sequelize");
const {getProvinceCityCode} = require('../util/apiUtil.js');
const {timeString, customWhere} = require('../util/util.js');
const moment = require('moment');
const user = require('./user.js');
const mangheUser = require('./mangheUser.js');

module.exports = {
    shenhe: defineModel('shenhe', {
        date: {
            type:  DataTypes.STRING,
            comment: '审核时间',
            get() {
                return moment(+this.getDataValue('date')).format('MM-DD HH:mm');
            }
        },
        content: {
            type: DataTypes.TEXT,
            allowNull: true,
            comment: '审核失败描述'
        },
        userId: {
            type:  DataTypes.STRING,
            comment: '数据用户ID'
        },
        contentId: {
            type: DataTypes.INTEGER,
            comment: '审核资源ID'
        },
        type: {
            type: DataTypes.INTEGER,
            comment: '数据类型：1：招工，2：找活，3：跳蚤市场，4：商家'
        }
    }),
    /**
     * 提交审核
     */
    insertShenhe(req, res, next) {
        let query = req.body;
        let {content, contentId, userId, type, shenheType, tags} = query;
        let obj = {
            content,
            contentId,
            userId,
            type,
            date: timeString()
        };
        // 更新业务表
        this.updateStatus(contentId, type, shenheType, tags)
            .then(() => {
                // 3 通过
                let status = shenheType == 3 ? true : false;
                if (status) {
                    mangheUser.addNum(userId);
                }
                this.sendNotice(userId, contentId, type, status, content);
                res.send({
                    code: 200,
                    msg: '操作成功'
                });
            })
            .catch((err) => {
                console.log('更新业务表失败insertShenhe', err);
                res.send({
                    code: 400,
                    msg: '操作失败'
                });
            });
        // 更新审核表
        this.shenhe.findAll({
            where: {
                type,
                contentId
            },
            raw: true
        })
            .then(selectRes => {
                // 审核通过
                if (shenheType == 3) {
                    // 如果有未审核通过的数据记录，则删除之前的记录
                    if (selectRes[0]) {
                        this.shenhe.destroy({
                            where: {
                                id: selectRes[0].id
                            }
                        });
                    }
                    return ;
                }
                if (selectRes[0]) {
                    // 审核不通过更新
                    this.shenhe.update(
                        obj,
                        {
                            where: {
                                id: selectRes[0].id
                            },
                            raw: true
                        },
                    )
                        .then(updateRes => {
                            console.log('更新审核表成功nsertShenh', updateRes)
                        })
                        .catch(updateErr => {
                            console.log('更新失败insertShenhe', updateErr);
                            res.send({
                                code: 400,
                                msg: '操作失败'
                            });
                        })
                }
                else {
                    // 审核不通过新增
                    this.shenhe.create(obj)
                    .then(result=> {
                        // this.updateStatus(contentId, type, 2)
                        //     .then(() => {
                        //     })
                        //     .catch(() => {
                        //         return Promise.reject();
                        //     });
                    })
                    .catch(err => {
                        console.log('插入失败insertShenhe', err);
                    })
                }
            })
            .catch(selectErr => {
                console.log('查询失败：insertShenhe', selectErr);
                res.send({
                    code: 400,
                    msg: '操作失败'
                });
            });
    },
    /**
     * 更新招工、找活、店铺、跳蚤市场的审核状态
     * @param {string} id 各表的id
     * @param {number} type 区分表 1：招工，2：找活，3：跳蚤市场，4：商家
     * @param {number} exam 2: 审核不通过，3：审核通过
     * @param {tags} tags 标签
     */
    updateStatus(id, type, exam, tags = '') {
        // 招工
        // if (type == 1) {
            let recruitment = require('./recruitment.js');
            return recruitment.updateExamineStatus(id, exam, type, tags);
        // }
        // // 找活
        // if (type == 2) {
        //     let findJob = require('./findJob.js');
        //     return findJob.updateExamineStatus(id, exam);
        // }
        // // 跳蚤市场
        // if (type == 3) {
        //     let market = require('./market.js');
        //     return market.updateExamineStatus(id, exam);
        // }
        // // 商家
        // if (type == 4) {
        //     let shop = require('./shop.js');
        //     return shop.updateExamineStatus(id, exam);
        // }
    },
    /**
     * 发送审核结果通知
     * @param {string} userId 用户id
     * @param {string} contentId 对应内容的id：招工、店铺等
     * @param {number} type 审核类型
     * @param {boolean} status 审核结果：true、false
     * @param {string} remarks 备注
     */
    async sendNotice(userId, contentId, type, status, remarks = '-') {
        const send = require('./send.js');
        const user  = require('./user');
        let statusStr = status ? '审核通过' : '审核失败';
        try {
            const userInfo = await user.getUserInfo(userId);
            if (userInfo && userInfo[0] && userInfo[0].openid) {
                let openid = userInfo[0].openid;
                // 招工
                if (type == 1) {
                    let path = `pages/recruitmentDetail/index?id=${contentId}&examineStatus=${status ? 3 : 2}`
                    send.sendapi(openid, statusStr, '您发布的招工', path, remarks);
                }
                // 找活
                if (type == 2) {
                    let path = `pages/jobDetail/index?id=${contentId}`
                    send.sendapi(openid, statusStr, '您发布的找活', path, remarks);
                }
                // 跳蚤市场
                if (type == 3) {
                    let path = `pages/marketDetail/index?id=${contentId}`
                    send.sendapi(openid, statusStr, '您发布的商品', path, remarks);
                }
                // 跳蚤市场
                if (type == 4) {
                    let path = `pages/shopDetail/index?id=${contentId}`
                    send.sendapi(openid, statusStr, '您创建的店铺', path, remarks);
                }
            }
        }
        catch(err) {
            console.log('发送审核结果通知时，获取用户信息失败sendNotice：', err);
        }
    },
     // 获取全部数据
    getAllList(req, res, next) {
        let where = 'where examineStatus=1';
         let {page, pageSize} = req.query;
         let limitStar = page * pageSize - pageSize;
         let sqlStr = `(select id, workDescribe as title, type, formattedAddress, phone, 1 as busnessType, FROM_UNIXTIME(date/1000,'%c月%e日 %h:%m') as time, date from recruitment ${where})
         UNION ALL
         (select id, introduce as title, type, formattedAddress, phone, 2 as busnessType, FROM_UNIXTIME(date/1000,'%c月%e日 %h:%m') as time, date from findJob ${where})
         UNION ALL
         (select id, title, type, formattedAddress, phone, 3 as busnessType, FROM_UNIXTIME(date/1000,'%c月%e日 %h:%m') as time, date from market ${where})
         UNION ALL
         (select id, showName as title, shopType as type, formattedAddress, phone, 4 as busnessType, FROM_UNIXTIME(date/1000,'%c月%e日 %h:%m') as time, date from shop ${where}) order by date asc limit ${limitStar}, ${pageSize}`;
         sequelize.query(sqlStr, {type: sequelize.QueryTypes.SELECT})
         .then(result => {
             res.send({
                 code: 200,
                 msg: '操作成功',
                 data: result
             });
         }).catch(err => {
             res.send({
                 code: 400,
                 msg: '操作失败',
                 data: ''
             });
             console.log('获取全部数据失败getAllList：', err);
         });
    },
    /**
     * 获取审核详情
     */
    getShenheDetail(req, res, next) {
        let query = req.query;
        let {contentId, type} = query;
        this.shenhe.findAll({
            where: {
                contentId,
                type
            },
            raw: true
        })
            .then(result => {
                res.send({
                    code: 200,
                    msg: '操作成功',
                    data: result
                });
            })
            .catch(err => {
                res.send({
                    code: 400,
                    msg: '操作失败',
                    data: ''
                });
            })
    }
 };