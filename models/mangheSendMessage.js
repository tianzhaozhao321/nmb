const {defineModel, sequelize} = require('../db');
const {DataTypes, Op } = require("sequelize");
const user = require('./user.js');
const manghe = require('./manghe.js');

module.exports = {
    mangheSendMessage: defineModel('mangheSendMessage', {
        openId: {
            type: DataTypes.STRING,
            comment: '用户openid'
        }
    }),
    insertMangheSendMessage(req, res, next) {
        let openId = req.headers['x-wx-openid'];
        this.mangheSendMessage.create({
            openId
        })
        .then()
        .catch()
    },
    sendMangheMessage(openid) {
        request({
            url: "http://api.weixin.qq.com/cgi-bin/message/subscribe/send",
            method: "POST",
            body: JSON.stringify({
                page: path,
                touser: openid,
                template_id: "yOKQvSRMZ0_CVhCoc8hrMD43wEMyM15-pHvkvCWovos",
                miniprogram_state: "developer", // 开发版 developer  正式版本 formal
                data: {
                    // 这里替换成自己的模板 ID 的详细事项，不要擅自添加或更改
                    // 按照 key 前面的类型，对照参数限制填写，否则都会发送不成功
                    // 审核结果
                    phrase1: {
                    value: result,
                    },
                    // 审核内容
                    thing2: {
                    value: content,
                    },
                    // 审核状态
                    thing17: {
                        value: "审核完成",
                    },
                    // 备注
                    thing7: {
                        value: remarks
                    }
                },
            }),
          },function(error,res){
            console.log('发送盲盒消息失败', error);
                if(error) reject(error)
                console.log(res.body);
            //   resolve(res.body)
        });
    },
    // 操作失败
    operationFail(res) {
        res.json({
            code: 400,
            msg: '操作失败'
        });
    },
    // 抽奖
    updateMangheUser(req, res, next) {
        let query = req.body;
        const {id, userId, newNum, avatarUrl, index, mangheShareUserId, nickName} = query;
        user.updateUserInfo(nickName, avatarUrl, userId);
        console.log(avatarUrl, userId, index + '用户信息')
        manghe.updateManghe(id, avatarUrl, userId, index, (status) => {
            if (status == 300) {
                res.send({
                    code: 300,
                    msg: '已被其他用户开奖',
                    data: {
                        id
                    }
                });
                return;
            }
            if (!status) {
                this.operationFail(res);
                return;
            }
            // 查询信息
            this.mangheUser.findAll({
                where: {
                    userId
                },
                raw: true
            })
            .then(result => {
                let num = result[0].num;
                let oldShareStatus = result[0].shareStatus;
                let sumNum = (num * 10 + newNum * 10) / 10;
                let orderNumber = result[0].orderNumber;
                if (orderNumber > 0) {
                    orderNumber = orderNumber - 1;
                }
                let shareStatus = 2;
                if (mangheShareUserId && mangheShareUserId !== 'undefined') {
                    shareStatus = 1;
                }
                // 更新盲盒信息
                this.mangheUser.update(
                    {
                        num: sumNum,
                        orderNumber,
                        shareStatus
                    },
                    {
                        where: {
                            userId
                        }
                    })
                    .then(result => {
                        // 存在分享ID并且未被邀请过，邀请人次数加一
                        if(mangheShareUserId && oldShareStatus == 2) {
                            this.addOrderNumber(mangheShareUserId, 1);
                        }
                        res.send({
                            code: 200,
                            msg: '操作成功',
                            data: sumNum
                        });
                    })
                    .catch(err => {
                        this.operationFail(res);
                    })
            })
            .catch(err => {
                this.operationFail(res);
                console.log('更新失败updateMangheUser：', err);
            });
        });
    },
    getMangheUser(req, res, next) {
        const {userId} = req.query;
        this.mangheUser.findAll({
            where: {
                userId
            },
            raw: true
        })
        .then((result) => {
            if (result && result[0]) {
                res.send({
                    code: 200,
                    msg: '操作成功',
                    data: result[0]
                });
            }
            else {
                this.insertMangheUser(userId);
                res.send({
                    code: 200,
                    msg: '操作成功',
                    data: {
                        num: 0,
                        orderNumber: 1
                    }
                });
            }
        })
        .catch(() => {
            this.operationFail(res);
        });
    },
    // 创建一条数据
    insertMangheUser(userId) {
        this.mangheUser.create({
            orderNumber: 1,
            userId,
            num: 0,
            shareStatus: 2
        })
        .then()
        .catch()
    },
    // 抽奖次数增加
    addOrderNumber(userId, addOrderNum) {
        // 查询信息
        this.mangheUser.findAll({
            where: {
                userId
            },
            raw: true
        })
        .then(result => {
            if (result && result[0]) {
                let orderNumber = result[0].orderNumber;
                    orderNumber = +orderNumber + addOrderNum;
                // 更新盲盒信息
                this.mangheUser.update(
                    {
                        orderNumber
                    },
                    {
                        where: {
                            userId
                        }
                    })
                    .then(result => {
                    })
                    .catch(err => {
                        console.log('更新失败addMangheUser：', err);
                    })
            }
            
        })
        .catch(err => {
            console.log('更新失败addMangheUser：', err);
        });
    }
}