/**
 * @file 用户模型
 */
const db = require('../db');
const {DataTypes} = require('sequelize');
let request = require('request');
const {timeFormat, customWhere} = require('../util/util.js');
// CREATE TABLE `user` (
//     `id` int(20) NOT NULL AUTO_INCREMENT,
//     `name` char(255) DEFAULT NULL,
//     `age` char(255) DEFAULT NULL,
//     `openId` varchar(255) DEFAULT NULL,
//     `recruitment` int(2) DEFAULT '0' COMMENT '是否有发布招工，0未发布，1已发布',
//     `findJob` int(2) DEFAULT '0' COMMENT '是否发布找活，0未发布，1已发布',
//     `shop` int(2) DEFAULT '0' COMMENT '是否有发布店铺，0没有，1有发布',
//     `market` int(2) DEFAULT '0' COMMENT '是否有发布商品，0没有，1有',
//     `avatar` varchar(255) DEFAULT NULL COMMENT '头像',
//     `integral` int(11) DEFAULT NULL COMMENT '用户积分',
//     PRIMARY KEY (`id`)
//   ) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

module.exports = {
    // 用户openid
    openid: '',
    // 数据模型
    user: db.defineModel('user', {
        name: {
            type: DataTypes.STRING,
            allowNull: true,
            comment: '用户昵称'
        },
        age: {
            type: DataTypes.STRING,
            allowNull: true,
            comment: '用户年龄'
        },
        phone: {
            type: DataTypes.STRING,
            comment: '用户手机号'
        },
        openid: {
            type: DataTypes.STRING,
            comment: '用户openid'
        },
        recruitment: {
            type: DataTypes.INTEGER(2),
            defaultValue: 0,
            comment: '是否有发布招工信息，如果有发布则显示最新的招工ID'
        },
        findJob: {
            type: DataTypes.INTEGER(2),
            defaultValue: 0,
            comment: '是否有发布找活信息，如果有发布则显示最新的找活ID'
        },
        shop: {
            type: DataTypes.INTEGER(2),
            defaultValue: 0,
            comment: '是否有发布店铺信息，如果有发布则显示最新的店铺ID'
        },
        market: {
            type: DataTypes.INTEGER(2),
            defaultValue: 0,
            comment: '是否有发布商品，如果有发布则显示最新的商品ID'
        },
        avatar: {
            type: DataTypes.STRING,
            comment: '用户头像',
            allowNull: true
        },
        integral: {
            type: DataTypes.INTEGER,
            comment: '用户积分'
        },
        latitude: {
            type:  DataTypes.STRING,
            comment: '纬度',
            allowNull: true
        },
        longitude: {
            type:  DataTypes.STRING,
            comment: '经度',
            allowNull: true
        },
        date: {
            type:  DataTypes.STRING,
            comment: '注册时间',
            allowNull: true
        }
    },
    {
        initialAutoIncrement: 138578
    }),

    /**
     * 根据用户的code获取用户信息，通常是注册的时候使用
     * 已废弃
     */
    async getUser(req, res, next) {
        let that = this;
        let {code, avatarUrl, shareId} = req.query;
        let url = `https://api.weixin.qq.com/sns/jscode2session?appid=wxf1d87fc650915093&secret=2ee9038b1381259d095c953324e4e38a&grant_type=authorization_code&js_code=${code}`;
        console.log(url);
        request(url, function (err, response, body) {
            /*
            * response 响应信息的集合
            **/
            if (err) {
                res.json({
                    code: 400,
                    msg: '兑换openId失败'
                });
            }
            console.log(response);
            console.log('response.body', response.body);
            let openId = JSON.parse(response.body).openid;
            console.log('openId', openId);
            that.openIdToUser(function (val) {
                res.send(val);
            }, openId, avatarUrl, shareId);
        });
    },
    async login(req, res, next) {
        let that = this;
        let {cloudid, shareId} = req.body;
        this.openid = req.headers['x-wx-openid'];
        const api = `http://api.weixin.qq.com/wxa/getopendata?openid=${this.openid}`;
        request(api, {
            method: 'POST',
            body: JSON.stringify({
                cloudid_list: cloudid, // 传入需要换取的 CloudID
            }),
            headers: {
                'Content-Type': 'application/json',
            },
        }, (err, resp, body) => {
            if (err) {
                res.json({
                    code: 400,
                    msg: '兑换手机号失败'
                });
            }
            try {
                console.log('JSON.parse(body)', JSON.parse(body));
                console.log('JSON.parse(body).data_list[0]', JSON.parse(body).data_list[0].json);
                let data = JSON.parse(body).data_list[0]; // 从回包中获取手机号信息
                console.log('data', data);
                console.log('JSON.parse(data.json)', JSON.parse(data.json));
                console.log('JSON.parse(data.json).data', JSON.parse(data.json).data);
                const phone = JSON.parse(data.json).data.phoneNumber;
                // 将手机号发送回客户端，此处仅供示例
                // 实际场景中应对手机号进行打码处理，或仅在后端保存使用
                that.openIdToUser(function (val) {
                    res.send(val);
                }, phone, '', shareId);
            } catch (error) {
                console.log('获取用户手机号失败', error);
                if (error) {
                    res.json({
                        code: 400,
                        msg: '创建用户失败'
                    });
                }
            }
        });
        // request({
        //     method: 'POST',
        //     url: 'http://api.weixin.qq.com/wxa/business/getuserphonenumber',
        //     body: JSON.stringify({
        //         code
        //     })
        //   },function (error, response) {
        //     if (error) {
        //         res.json({
        //             code: 400,
        //             msg: error
        //         });
        //     }
        //     res.json({
        //         code: 200,
        //         msg: response.body
        //     });
        //         console.log('接口返回内容', response.body)
        //         // resolve(JSON.parse(response.body))
        //   });
    },
    login1(req, res, next) {
        let {cloudid, shareId} = req.query;
        let phone = '15810525430';
        this.openIdToUser(function (val) {
            res.send(val);
        }, phone, '', '');
    },
    openIdToUser(cb, phone, avatarUrl, shareId) {
        this.user.findAll({
            where: {
                phone
            },
            raw: true
        })
            .then(async result => {
                if (result && result[0]) {
                    try {
                        // 获取收藏总数
                        let conllection = require('./conllection');
                        let conllectionCount = await conllection.conllectionCount(result[0].id);
                        result[0].conllectionCount = conllectionCount;
                        // 获取联系总数
                        let telPhone = require('./telPhone.js');
                        let telPhoneCount = await telPhone.telPhoneCount(result[0].id);
                        result[0].telPhoneCount = telPhoneCount;
                        cb({
                            code: 200,
                            data: result
                        });
                    }
                    catch (err) {
                        cb({
                            code: 400,
                            data: '',
                            msg: '获取用户收藏总数失败'
                        });
                        console.log('获取用户收藏总数失败openIdToUser,conllectionCount', err);
                    }
                    return;
                }
                this.addUser(cb, phone, shareId);
            })
            .catch(err => {
                console.log('查询用户失败：openIdToUser', err);
            });
    },
    addUser(cb, phone, shareId) {
        console.log('创建用户');
        this.user.create({
            phone,
            integral: 2,
            openid: this.openid,
            date: timeFormat()
        })
            .then(result => {
                this.updateIntegral(shareId);
                if (result.dataValues && result.dataValues.id) {
                    this.user.findAll({
                        where: {
                            id: result.dataValues.id
                        }
                    })
                        .then(result1 => {
                            cb({
                                code: 200,
                                data: result1
                            });
                        })
                        .catch(err => {
                            cb({
                                code: 400,
                                data: '',
                                msg: '查询用户失败'
                            });
                            console.log('查询用户失败: addUser', err);
                        });
                }
            })
            .catch(err => {
                cb({
                    code: 400,
                    data: '',
                    msg: '创建用户失败'
                });
                console.log('创建用户失败:addUser', err);
            });
    },
    /**
     * 获取用户信息
     */
    getUserInfo(userId) {
        return this.user.findAll({
            where: {
                id: userId
            },
            raw: true
        })
            .then(result => {
                return result;
            })
            .catch(err => {
                Promise.reject(err);
            });
    },
    /**
     * 根据openid获取用户信息
     * @param {*} openid 
     * @returns 
     */
    getUserInfoFromOpenId(openid) {
        return this.user.findAll({
            where: {
                openid: openid
            },
            raw: true
        })
            .then(result => {
                return result;
            })
            .catch(err => {
                Promise.reject(err);
            });
    },

    /**
     * 更新用户的店铺、招工、找活、跳蚤市场等信息
     */
    updataUserInfo(serviceId, userId, type) {
        let obj = {};
        if (type === 'market') {
            obj.market = serviceId;
        }
        if (type === 'recruitment') {
            obj.recruitment = serviceId;
        }
        if (type === 'findJob') {
            obj.findJob = serviceId;
        }
        if (type === 'shop') {
            obj.shop = serviceId;
        }
        return this.user.update(
            obj,
            {
                where: {
                    id: userId
                }
            })
            .then(result => {
                console.log('用户表更新成功', obj, userId);
                return true;
            })
            .catch(err => {
                console.log('更新用户的店铺、招工、找活、跳蚤市场等信息失败updataUserInfo:', err);
                return Promise.reject(false);
            });
    },
    /**
     * 根据用户id和手机号获取用户信息
     */
    getUserInfoFromId(req, res, next) {
        let {id, phone} = req.query;
        this.user.findAll({
            where: {
                id,
                phone
            },
            raw: true
        })
            .then(async result => {
                try {
                    // 获取收藏总数
                    let conllection = require('./conllection');
                    let conllectionCount = await conllection.conllectionCount(result[0].id);
                    result[0].conllectionCount = conllectionCount;
                    // 获取联系总数
                    let telPhone = require('./telPhone.js');
                    let telPhoneCount = await telPhone.telPhoneCount(result[0].id);
                    result[0].telPhoneCount = telPhoneCount;
                    res.send({
                        code: 200,
                        msg: '操作成功',
                        data: {
                            result
                        }
                    });
                }
                catch(err) {
                    res.json({
                        code: 400,
                        msg: '获取失败'
                    });
                    console.log('获取用户收藏总数失败openIdToUser,conllectionCount', err,)
                }
            })
            .catch(err => {
                console.log('获取用户信息失败getUserInfoFromId', err);
                res.json({
                    code: 400,
                    msg: '获取失败'
                });
            });
    },
    /**
     * 减少积分
     */
    reduceIntegral(userId, integral) {
        this.user.update({
            integral: integral
        },
        {
            where: {
                id: userId
            }
        })
        .then(result => {
            console.log('减少用户积分成功', result);
        })
        .catch(updateErr => {
            console.log('减少用户积分失败', updateErr);
        });
    },
    /**
     * 更新用户积分
     *
     * @param {number} id 用户id
     */
    updateIntegral(id) {
        if (id) {
            this.user.findAll({
                where: {
                    id
                },
                raw: true
            })
                .then(res => {
                    let integral = res[0].integral;
                    this.user.update({
                        integral: integral + 1
                    },
                    {
                        where: {
                            id
                        }
                    })
                        .then(result => {
                            console.log('分享用户积分更新成功updateIntegral', result);
                        })
                        .catch(updateErr => {
                            console.log('分享用户积分更新失败updateIntegral', updateErr);
                        });
                })
                .catch(err => {
                    console.log('分享用户积分更新“查询”失败updateIntegral', err);
                });
        }
    },
    // 更新用户头像，昵称
    updateUserInfo(name, avatar, userId) {
        this.user.update(
            {
                name,
                avatar
            },
            {
                where: {
                    id: userId
                }
            });
    },
    // 更新用户的经纬度
    updateLatLong(req, res) {
        let {userId, latitude, longitude} = req.query;
        this.user.update({
            latitude,
            longitude
        },{
            where: {
                id: userId
            }
        })
        .then(result => {
            res.send({
                code: 200,
                msg: '操作成功',
                data: result
            });
        })
        .catch(err => {
            res.send({
                code: 400,
                msg: '操作失败',
                data: err
            });
            console.log('更新用户经纬度信息失败updateLatLong:', err);
        })
    },
    // 生成二维码
    svg(req, res, next) {
        let qrSvg = require('./qr-image.js');
        let svgStr = qrSvg.qrSvg('你好');
        console.log(svgStr);
        res.send({
            code: 200,
            msg: '操作成功',
            data: svgStr
        });
    }
};
