/**
 * 发送订阅消息
 */
const request = require("request");
module.exports = {
    send(req, res,) {
        let openid = req.headers['x-wx-openid'];
        console.log('发送订阅');
        this.sendapi(openid);
    },
    /**
     * 
     * @param {string} openid 用户openid
     * @param {string} result 审核通过、审核不通过
     * @param {string} content 申请的内容：您创建的店铺、您发布的招工、您发布的找活、您发布的商品
     * @param {string} path 小程序要跳转的页面
     * @param {string} remarks 备注
     * 文档地址： https://developers.weixin.qq.com/miniprogram/dev/api-backend/open-api/subscribe-message/subscribeMessage.send.html
     */
    async sendapi(openid, result, content, path, remarks) {
		console.log('发送订阅~~~~~', {
				page: path,
				touser: openid,
				template_id: "yOKQvSRMZ0_CVhCoc8hrMD43wEMyM15-pHvkvCWovos",
				miniprogram_state: "developer"
		});
        return new Promise((resolve, reject) => {
            request({
                url: "http://api.weixin.qq.com/cgi-bin/message/subscribe/send",
                method: "POST",
                body: JSON.stringify({
                    page: path,
                    touser: openid,
                    template_id: "yOKQvSRMZ0_CVhCoc8hrMD43wEMyM15-pHvkvCWovos",
                    miniprogram_state: "formal", // 开发版 developer  正式版本 formal
                    data: {
                        // 这里替换成自己的模板 ID 的详细事项，不要擅自添加或更改
                        // 按照 key 前面的类型，对照参数限制填写，否则都会发送不成功
                        // 审核结果
                        phrase1: {
                        value: result,
                        },
                        // 审核内容
                        thing2: {
                        value: content,
                        },
                        // 审核状态
                        thing17: {
                            value: "审核完成",
                        },
                        // 备注
                        thing7: {
                            value: remarks
                        }
                    },
                }),
            },function(error,res){
                console.log('发送订阅消息失败--审核结果', error);
                if(error) reject(error)
                console.log(res.body);
                //   resolve(res.body)
            });
        });
    },
    async sendMangheApi(openid, num) {
        console.log('发送盲盒订阅~~~~~', {
            page: 'pages/manghe/index',
            touser: openid,
            template_id: "3aF50ZuRPzDosyltGGq0TwcDnCKhHPkBn1aI31p0QHw",
            miniprogram_state: "developer"
        })
        return new Promise((resolve, reject) => {
            request({
                url: "http://api.weixin.qq.com/cgi-bin/message/subscribe/send",
                method: "POST",
                body: JSON.stringify({
                    page: 'pages/manghe/index',
                    touser: openid,
                    template_id: "3aF50ZuRPzDosyltGGq0TwcDnCKhHPkBn1aI31p0QHw",
                    miniprogram_state: "formal", // 开发版 developer  正式版本 formal
                    data: {
                        // 这里替换成自己的模板 ID 的详细事项，不要擅自添加或更改
                        // 按照 key 前面的类型，对照参数限制填写，否则都会发送不成功
                        // 活动名称
                        thing21: {
                            value: '抽盲盒，赢取百元大钞',
                        },
                        // 温馨提醒
                        thing18: {
                            value: `您当前有${num}次机会未使用`,
                        }
                    },
                }),
            },function(error,res){
                if(error)  {
                    console.log('发送盲盒订阅消息失败', error);
                    reject(error)
                }
                else {
                    console.log('发送盲盒订阅消息成功', res.body);
                }
                //   resolve(res.body)
            });
        });
    },
    // 更新消息
    updataMessage(req, res) {
        
    }
};
