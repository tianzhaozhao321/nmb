/**
 * 找活模型
 */
//  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
//  `userId` int(20) NOT NULL COMMENT '用户ID',
//  `name` varchar(100) NOT NULL COMMENT '姓名',
//  `gender` varchar(10) NOT NULL COMMENT '男、女',
//  `birthday` varchar(255) NOT NULL COMMENT '出生日期',
//  `type` varchar(255) NOT NULL COMMENT '期望工种',
//  `phone` varchar(40) NOT NULL COMMENT '联系电话',
//  `introduce` text NOT NULL COMMENT '自我介绍',
//  `date` varchar(20) NOT NULL COMMENT '新增或编辑日期',
//  `adcode` varchar(10) NOT NULL COMMENT '县级编码',
//  `latitude` varchar(40) NOT NULL COMMENT '维度',
//  `longitude` varchar(40) NOT NULL COMMENT '精度',
//  `formattedAddress` varchar(255) NOT NULL COMMENT '定位地址',
//  `cityCode` varchar(20) NOT NULL COMMENT '市',
//  `provinceCode` varchar(20) NOT NULL COMMENT '省',

const {defineModel, sequelize} = require('../db');
const {DataTypes } = require("sequelize");
const {getProvinceCityCode} = require('../util/apiUtil.js');
const {timeString, customWhere} = require('../util/util.js');
const moment = require('moment');
const user = require('./user.js');

module.exports = {
    findJob: defineModel('findJob', {
        introduce: {
            type: DataTypes.TEXT,
            comment: '自我介绍'
        },
        userId: {
            type: DataTypes.INTEGER,
            comment: '用户ID'
        },
        name: {
            type: DataTypes.STRING,
            comment: '姓名'
        },
        gender: {
            type:  DataTypes.STRING,
            comment: '男、女'
        },
        birthday: {
            type:  DataTypes.STRING,
            comment: '出生日期'
        },
        type: {
            type:  DataTypes.STRING,
            comment: '期望工种'
        },
        adcode: {
            type:  DataTypes.STRING,
            comment: '县级编码'
        },
        latitude: {
            type:  DataTypes.STRING,
            comment: '纬度'
        },
        longitude: {
            type:  DataTypes.STRING,
            comment: '精度'
        },
        phone: {
            type:  DataTypes.STRING,
            comment: '联系电话'
        },
        date: {
            type:  DataTypes.STRING,
            comment: '发布时间',
            get() {
                return moment(this.getDataValue('create_time')).format('YYYY-MM-DD HH:mm:ss');
            }
        },
        images: {
            type: DataTypes.TEXT,
            allowNull: true,
            comment: '图片'
        },
        formattedAddress: {
            type:  DataTypes.STRING,
            comment: '格式化后的地址'
        },
        cityCode: {
            type:  DataTypes.STRING,
            comment: '城市编码'
        },
        provinceCode: {
            type:  DataTypes.STRING,
            comment: '省编码'
        },
        examineStatus: {
            type: DataTypes.INTEGER(2),
            comment: '审核状态，1待审核，2审核不通过，3审核通过'
        }
    }),
    async insertFindJob(req, res, next) {
        let query = req.body;
        const {id, name, gender, birthday, type, phone, introduce, userId, adcode, latitude, longitude, formattedAddress} = query;

        // 添加测试数据 ?name=田&gender=1&birthday=2022-04-07&type=技工&phone=phone&introduce=第一份工作&userId=1&adcode=110108&formattedAddress=北京市海淀区长春桥路17号-海淀区人民政府内&latitude=39.95933&longitude=116.29845
        try {
            let pcValue = await getProvinceCityCode(adcode);
            let obj = {
                name,
                birthday,
                type,
                phone,
                gender,
                introduce,
                userId,
                adcode,
                latitude,
                longitude,
                formattedAddress,
                cityCode: pcValue.cityCode,
                provinceCode: pcValue.provinceCode,
                examineStatus: 1,
                date: timeString()
            }
            if (id) {
                obj.id = id;
                // 更新
                this.findJob.update(
                    obj,
                    {
                        where: {
                            id
                        }
                    })
                    .then(result => {
                        res.send({
                            code: 200,
                            msg: '操作成功',
                            data: {
                                id
                            }
                        })
                    })
                    .catch(err => {
                        console.log('更新失败insertFindJob：', err);
                    })
            }
            else {
                // 新增
                this.findJob.create(obj)
                .then(result=> {
                    user.updataUserInfo(result.dataValues.id, userId, 'findJob')
                        .then(result1 => {
                            res.send({
                                code: 200,
                                msg: '操作成功',
                                data: {
                                    id: result.dataValues.id
                                }
                            });
                            console.log('插入成功', res);
                        })
                        .catch(err1 => {
                            res.json({
                                code: 400,
                                msg: '新增失败'
                            });
                            console.log('插入失败:insertMarket', err1);
                        });
                })
                .catch(err => {
                    res.json({
                        code: 400,
                        msg: '新增失败'
                    });
                    console.log('插入失败', err);
                });
            }
        }
        catch(err) {
            console.log('获取省市编码错误insertFindJob：', err);
            res.send({
                code: 400,
                msg: '获取省市编码错误',
                data: {}
            });
        };
    },
    /**
     * 根据ID查询简历
     */
     getFindJob(req, res, next) {
        let query = req.query;
        const {id} = query;
        this.findJob.findAll({
            where: {
                id
            }
        })
            .then(result => {
                res.send({
                    code: 200,
                    msg: '操作成功',
                    data: result
                });
            })
            .catch(err => {
                console.log('获取简历详情失败getFindJob：', err);
                res.send({
                    code: 400,
                    msg: '操作失败',
                    data: err
                });
            });
    },
    /**
     * 根据userId查询简历
     */
    getFindJobUser(req, res, next) {
        let query = req.query;
        const {userId} = query;
        this.findJob.findAll({
            where: {
                userId
            }
        })
            .then(result => {
                res.send({
                    code: 200,
                    msg: '操作成功',
                    data: result
                });
            })
            .catch(err => {
                res.json({
                    code: 400,
                    msg: '操作失败',
                    data: err
                });
                console.log('根据用户ID获取简历失败getFindJobUser:', err);
            })
        let sql = `select * from findJob where userId='${userId}'`;
    },
    /**
     * 获取全部招工列表
     */
    getFindJobList(req, res, next) {
        let {page, pageSize, type} = req.query;
        let limitStar = page * pageSize - pageSize;
        let where = customWhere(req.query);
        if (type && type !== '全部') {
            where.shopType = type;
        }
        this.findJob.findAll({
            attributes: ['id', ['introduce', 'title'], 'type', 'formattedAddress', 'phone', 'date'],
            where: {
                examineStatus: 3,
                ...where
            },
            order: [['date', 'desc']],
            offset: limitStar || 0,
            limit: +pageSize,
            raw: true
        })
            .then(result => {
                res.send({
                    code: 200,
                    msg: '操作成功',
                    data: result.map(val => {
                        val.busnessType = 2;
                        return val;
                    })
                });
            })
            .catch(err => {
                console.log('获取全部简历失败getFindJobList：', err);
                res.send({
                    code: 400,
                    msg: '操作失败',
                    data: err
                });
            });
    },
    /**
     * 根据用户id和找活ID更新详情
     */
    findJobDetail(req, res, next) {
        let {id, userId} = req.query;
        this.findJob.findAll({
            where: {
                id,
                userId
            }
        })
            .then(result => {
                // getUserInfo
                user.getUserInfo(userId)
                .then(resPro => {
                    result[0].avatar = resPro.avatar
                    res.send({
                        code: 200,
                        msg: '操作成功',
                        data: result
                    });
                })
                .catch(err => {
                    console.log('根据用户id和找活ID更新详情失败user.getUserInfo', err);
                    res.send({
                        code: 400,
                        msg: '操作失败',
                        data: err
                    });
                });
            })
            .catch(err => {
                res.send({
                    code: 400,
                    msg: '操作失败',
                    data: err
                });
                console.log('根据用户id和找活ID更新详情失败findJobDetail', err);
            });
    },
    // 更新找活审核状态
    updateExamineStatus(id, status) {
        return this.findJob.update({
            examineStatus: status
        },
        {
            where: {
                id
            }
        })
            .then(res => {
                return res;
            })
            .catch(err => {
                console.log('更新找活失败updateExamineStatus', err)
                return Promise.reject(err);
            })
    }
}