/**
 * 商品类型模型
 */
//  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
//  `value` int(20) NOT NULL COMMENT '商品类型的索引',
//  `name` varchar(100) NOT NULL COMMENT '商品类型的名称',
//  PRIMARY KEY (`id`)
const db = require('../db');
const {DataTypes } = require("sequelize");
module.exports = {
    transactionType: db.defineModel('transactionType', {
        name: {
            type: DataTypes.STRING,
            allowNull: true,
            comment: '商品类型的名称'
        },
        value: {
            type: DataTypes.INTEGER,
            comment: '商品类型的编码'
        }
    }),
    // 商品类型
    getTransactionType(req, res, next) {
        this.transactionType.findAll()
            .then(result => {
                res.send({
                    code: 200,
                    msg: '操作成功',
                    data: result
                });
            })
            .catch(err => {
                console.log('获取商品类型失败getTransactionType：', err);
                res.json({
                    code: 400,
                    msg: '操作失败',
                    data: err
                })
            });
    },
}