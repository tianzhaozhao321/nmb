const {defineModel, sequelize} = require('../db');
const {DataTypes } = require("sequelize");
const {getProvinceCityCode} = require('../util/apiUtil.js');
const {timeString, customWhere} = require('../util/util.js');
const moment = require('moment');
const user = require('./user.js');
const { updateExamineStatus } = require('./recruitment');

module.exports = {
    zhaoPinStatus: defineModel('zhaoPinStatus', {
        status: {
            type: DataTypes.STRING,
            comment: '是否显示招聘信息，1显示招聘信息，2不显示'
        },
        weihu: {
            type: DataTypes.STRING,
            comment: '发布页面是否显示，1显示，2不显示'
        }
    }),
    getZhaoPinStatus() {
        return this.zhaoPinStatus.findAll({
            where: {
                id: 1
            }
        })
        .then(result => {
            return result[0].status;
        })
        .catch(err => {
            console.log('获取招聘状态失败getZhaopinStatus', err);
        })
    },
    getZhaoPinStatusurl(req, res) {
        return this.zhaoPinStatus.findAll({
            where: {
                id: 1
            },
            raw: true
        })
        .then(result => {
            res.send({
                code: 200,
                msg: '操作成功',
                data: result[0]
            })
        })
        .catch(err => {
            console.log('获取招聘状态失败getZhaopinStatus', err);
        })
    },
    updateStatus(req, res) {
        let {type} = req.query;
        let obj = {};
        if (type == 1) {
            obj = {
                status: 2,
                weihu: 2
            }
        }
        else {
            obj = {
                status: 1,
                weihu: 1
            }
        }
        this.zhaoPinStatus.update(
            obj,
            {
                where: {
                    id: 1
                }
            })
            .then(result => {
                res.send({
                    code: 200,
                    data: result
                });
            })
            .catch(err => {
                res.send({
                    code: 400,
                    data: err
                });
            })
    }
}