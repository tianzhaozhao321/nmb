/**
 * 领取活动模型
 */

const {defineModel, sequelize} = require('../db');
const {DataTypes, Op } = require("sequelize");
const {timeString} = require('../util/util.js');
const moment = require('moment');

module.exports = {
    activityUser: defineModel('activityUser', {
        userId: {
            type: DataTypes.INTEGER,
            comment: '用户ID'
        },
        activityId: {
            type: DataTypes.TEXT,
            comment: '活动ID'
        },
        date: {
            type:  DataTypes.STRING,
            comment: '领取时间',
            get() {
                return moment(this.getDataValue('create_time')).format('YYYY-MM-DD HH:mm:ss');
            }
        },
        use: {
            type: DataTypes.BOOLEAN,
            default: false,
            comment: '是否已使用'
        },
        expirationTime: {
            type:  DataTypes.STRING,
            comment: '过期时间',
            get() {
                return (timeString() > this.getDataValue('expirationTime')) ? false : true;
            }
        },
    }),
    /**
     * 新增活动
     */
    insertActivityUser(req, res, next) {
        let query = req.body;
        const {userId, activityId, expirationTime, contentId} = query;
        this.activityUser.findAll({
            where: {
                userId,
                activityId
            },
            raw: true
        })
            .then(result => {
                if (result[0]) {
                    res.send({
                        code: 200,
                        msg: '操作成功',
                        data: {
                            id: result[0].id
                        }
                    });
                }
                else {
                    let obj = {
                        userId,
                        activityId,
                        expirationTime,
                        use: false,
                        date: timeString()
                    }
                    // 新增
                    this.activityUser.create(obj)
                    .then(result=> {
                        let activity = require('./activity');
                        activity.activity.update({
                            receive: sequelize.literal('`receive`+1'),
                            activityId
                        },
                        {
                            where: {
                                id: contentId
                            }
                        })
                        res.send({
                            code: 200,
                            msg: '操作成功',
                            data: {
                                id: result.dataValues.id
                            }
                        });
                        console.log('插入成功', res);
                    })
                    .catch(err => {
                        res.json({
                            code: 400,
                            msg: '新增失败'
                        });
                        console.log('插入新增活动失败', err);
                    });
                }
            })
            .catch(err => {
                console.log('插入新增活动失败', err);
            });
    },
    /**
     * 查询领取的活动
     */
    getActivityUser(req, res, next) {
        let query = req.query;
        const {userId} = query;
        this.activityUser.findAll({
            where: {
                userId
            },
            raw: true
        })
            .then(result => {
                res.send({
                    code: 200,
                    msg: '操作成功',
                    data: result
                });
            })
            .catch(err => {
                console.log('获取领取活动失败getActivityUser：', err);
                res.send({
                    code: 400,
                    msg: '操作失败',
                    data: err
                });
            });
    },
    /**
     * 查询是否已领取活动
     */
    getActivityUserStatus(req, res, next) {
        let query = req.query;
        const {userId, activityId} = query;
        this.activityUser.findAll({
            where: {
                id,
                activityId
            },
            raw: true
        })
            .then(result => {
                if (result[0]) {
                    res.send({
                        code: 200,
                        msg: '操作成功',
                        data: true
                    });
                }
                else {
                    res.send({
                        code: 200,
                        msg: '操作成功',
                        data: false
                    });
                }
                
            })
            .catch(err => {
                console.log('获取领取活动失败getActivityUser：', err);
                res.send({
                    code: 400,
                    msg: '操作失败',
                    data: err
                });
            });
    },
    /**
     * 获取活动使用二维码
     */
    getActivitySvg(req, res, next) {
        let query = req.query;
        const {id} = query;
        let qrSvg = require('./qr-image.js');
        let svgStr = qrSvg.qrSvg(`?id=${id}`);
        res.send({
            code: 200,
            msg: '操作成功',
            data: svgStr
        });
    },
    /**
     * 扫码
     */
    scanCode(req, res, next) {
        let query = req.query;
        const {id} = query;
        this.activityUser.findAll({
            where: {
                id
            },
            raw: true
        })
            .then(result => {
                
                // 二维码未使用
                if (!result[0].use) {
                    this.activityUser.update(
                        {
                            use: true,
                        },
                        {
                            where: {
                                id
                            }
                        }
                    )
                        .then(result1 => {
                            // 更新领取个数
                            let activity = require('./activity');
                            activity.activity.update({
                                useNumber: sequelize.literal('`useNumber`+1'),
                            },
                            {
                                where: {
                                    id: result[0].activityId
                                }
                            });

                            res.send({
                                code: 200,
                                msg: '操作成功',
                                data: result1
                            });
                        })
                        .catch(err => {
                            res.send({
                                code: 400,
                                msg: '二维码不存在，请重试',
                                data: err
                            });
                            console.log('更新二维码不存在请重试：scanCode：', err);
                        });
                }
            })
            .catch(err => {
                res.send({
                    code: 400,
                    msg: '二维码不存在，请重试',
                    data: err
                });
                console.log('二维码不存在请重试：scanCode：', err);
            });
    }
}