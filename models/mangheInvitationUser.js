const {defineModel, sequelize} = require('../db');
const {DataTypes, Op } = require("sequelize");
const user = require('./user.js');
const manghe = require('./manghe.js');
const send = require('./send.js');
const {timeFormat, customWhere} = require('../util/util.js');

module.exports = {
    mangheInvitationUser: defineModel('mangheInvitationUser', {
        userId: {
            type:  DataTypes.STRING,
            comment: '用户ID'
        },
        name: {
            type: DataTypes.STRING,
            comment: '用户昵称',
            allowNull: true
        },
        invitationUser: {
            type: DataTypes.STRING,
            comment: '受邀用户ID'
        },
        date: {
            type:  DataTypes.STRING,
            comment: '邀请时间'
        }
    }),
    findUserId(userId) {
        return this.mangheInvitationUser.findAll({
            where: {
                userId
            },
            raw: true
        })
        .then(result => {
            return result;
        })
        .catch(err => {
            console.log('受邀用户查询失败findUserId:', err);
            return err;
        })
    },
    // 根据用户ID，受邀用户ID查询数据
    findUserIdInvitationUser(userId, invitationUser) {
        return this.mangheInvitationUser.findAll({
            where: {
                userId,
                invitationUser
            },
            raw: true
        })
        .then(result => {
            return result;
        })
        .catch(err => {
            console.log('根据用户ID，受邀用户ID查询数据失败findUserIdInvitationUser:', err);
            return err;
        })
    },
    // 创建一条数据
    async insertMangheInvitationUser(userId, invitationUser) {
        let userInfo = await user.getUserInfo(userId);
        this.mangheInvitationUser.create({
            userId,
            invitationUser,
            name: userInfo.name,
            date: timeFormat()
        })
        .then()
        .catch()
    },
    // 更新时间戳
    updateUserInvitationUser(userId, invitationUser) {
        this.mangheInvitationUser.update(
            {
                date: timeFormat()
            },
            {
                where: {
                    userId,
                    invitationUser
                }
            })
            .then(result => {
                console.log('更新成功updateUserInvitationUser');
            })
            .catch(err => {
                console.log('更新失败：updateUserInvitationUser', err);
            })
    },
    // 邀请的用户参与完成抽奖
    async updateUser(userId, invitationUser) {
        let user = await this.findUserIdInvitationUser(userId, invitationUser);
        if (user.length) {
            this.updateUserInvitationUser(userId, invitationUser);
        }
        else {
            this.insertMangheInvitationUser(userId, invitationUser);
        }
        // SELECT * FROM recruitment WHERE DATEDIFF(date, NOW())=0;
    },
    // 今天数据
    getShareRanking(req, res) {
        let sqlStr = 'SELECT userId,COUNT(*) as count from mangheInvitationUser WHERE DATEDIFF(date, NOW())=0 GROUP BY userId ORDER BY count DESC LIMIT 20';
        sequelize.query(sqlStr, {type: sequelize.QueryTypes.SELECT})
            .then(result => {
                res.send({
                    code: 200,
                    msg: '操作成功',
                    data: result
                });
            })
            .catch(err => {
                res.send({
                    code: 400,
                    msg: '操作成功',
                    data: err
                });
                console.log('查询排名失败getShareRanking:', err);
            });
    },
    // 前一天数据
    getShareRankingYesterday(req, res) {
        let sqlStr = 'SELECT userId,COUNT(*) as count from mangheInvitationUser WHERE DATEDIFF(date, NOW())=-1 GROUP BY userId ORDER BY count DESC LIMIT 20';
        sequelize.query(sqlStr, {type: sequelize.QueryTypes.SELECT})
            .then(result => {
                res.send({
                    code: 200,
                    msg: '操作成功',
                    data: result
                });
            })
            .catch(err => {
                res.send({
                    code: 400,
                    msg: '操作成功',
                    data: err
                });
                console.log('查询排名失败getShareRanking:', err);
            });
    },
}