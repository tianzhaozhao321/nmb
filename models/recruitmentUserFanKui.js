/**
 * @file 招工模型
 */
const {defineModel, sequelize} = require('../db');
const {DataTypes } = require("sequelize");
const {timeString, timeFormat, timeFormatM} = require('../util/util.js');
const user = require('./user.js');

// DROP TABLE IF EXISTS `recruitment`;
// CREATE TABLE `recruitment` (
//   `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
//   `workDescribe` text NOT NULL COMMENT '招工详情',
//   `contacts` varchar(100) NOT NULL COMMENT '联系人',
//   `phone` varchar(30) NOT NULL COMMENT '手机号',
//   `formattedAddress` varchar(255) NOT NULL COMMENT '定位地址',
//   `latitude` varchar(40) NOT NULL COMMENT '维度',
//   `longitude` varchar(40) NOT NULL COMMENT '精度',
//   `adcode` varchar(10) NOT NULL COMMENT '县级编码',
//   `type` varchar(255) NOT NULL COMMENT '店铺类型',
//   `date` varchar(20) NOT NULL COMMENT '新增或编辑日期',
//   `images` text NOT NULL COMMENT '图片列表',
//   `examineStatus` int(2) NOT NULL COMMENT '审核状态，1待审核，2审核不通过，3审核通过',
//   `userId` int(11) NOT NULL COMMENT '用户id',
//   `cityCode` varchar(20) NOT NULL COMMENT '市',
//   `provinceCode` varchar(20) NOT NULL COMMENT '省',
//   PRIMARY KEY (`id`)
// ) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COMMENT='招工表';

module.exports = {
    recruitmentUserFanKui: defineModel('recruitmentUserFanKui', {
        workDescribe: {
            type: DataTypes.TEXT,
            comment: '招工描述'
        },
        contentId: {
            type: DataTypes.STRING,
            comment: '招工信息id'
        },
        fankuiUserId: {
            type: DataTypes.STRING,
            comment: '反馈问题的用户ID'
        },
        contentUserId: {
            type:  DataTypes.STRING,
            comment: '发布信息的用户id'
        },
        type: {
            type:  DataTypes.STRING,
            comment: '反馈结果 1、联系上，2、未联系上了'
        },
        miaoshu: {
            type: DataTypes.TEXT,
            allowNull: true,
            comment: '反馈结果的描述'
        }
    }),
    // 新增一条反馈
    async insertRecruitmentUserFanKui(req, res, next) {
        let {workDescribe, contentId, fankuiUserId, contentUserId, type, miaoshu} = req.body;
        this.recruitmentUserFanKui.create({
            workDescribe,
            contentId,
            fankuiUserId,
            contentUserId,
            type,
            miaoshu
        })
        .then(result=> {
            res.send({
                code: 200,
                msg: '操作成功',
                data: result
            });
        })
        .catch(err => {
            console.log('创建用户反馈数据失败', err);
        })
    },
    // 返回反馈信息
    getFanKui(req, res, next) {
        let {type, contentId, contentUserId} = req.query;
        let obj = {};
        if (type) {
            obj.type = type;
        }
        if (contentUserId) {
            obj.contentUserId = contentUserId;
        }
        if (fankuiUserId) {
            obj.fankuiUserId = fankuiUserId;
        }
        if (contentId) {
            obj.contentId = contentId;
        }
        this.recruitmentUserFanKui.findAll({
            where: {
                obj
            }
        })
        .then(result => {
            res.send({
                code: 200,
                msg: '操作成功',
                data: result
            });
        })
        .catch(err => {
            res.send({
                code: 400,
                msg: '操作失败',
                data: err
            });
        })
    }
};
