/**
 * 城市模型
 */

const {defineModel, sequelize} = require('../db');
const {DataTypes } = require("sequelize");
module.exports = {
    city: defineModel('recruitment', {
        CITY_CODE: {
            type: DataTypes.STRING,
            comment: '市代码'
        },
        CITY_NAME: {
            type: DataTypes.STRING,
            comment: '市名称'
        },
        SHORT_NAME: {
            type:  DataTypes.STRING,
            comment: '简称'
        },
        PROVINCE_CODE: {
            type:  DataTypes.STRING,
            comment: '省代码'
        },
        LNG: {
            type:  DataTypes.STRING,
            comment: '精度'
        },
        LAT: {
            type:  DataTypes.STRING,
            comment: '纬度'
        }
    })
};
