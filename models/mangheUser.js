const {defineModel, sequelize} = require('../db');
const {DataTypes, Op } = require("sequelize");
const user = require('./user.js');
const manghe = require('./manghe.js');
const send = require('./send.js');
const mangheInvitationUser = require('./mangheInvitationUser.js');

module.exports = {
    mangheUser: defineModel('mangheUser', {
        orderNumber: {
            type: DataTypes.STRING,
            comment: '剩余抽奖次数'
        },
        userId: {
            type:  DataTypes.STRING,
            comment: '用户ID'
        },
        num: {
            type: DataTypes.STRING,
            comment: '余额'
        },
        useNum: {
            type: DataTypes.STRING,
            comment: '已提现金额',
            allowNull: true,
        },
        raffleNum: {
            type: DataTypes.STRING,
            comment: '抽奖次数',
            allowNull: true
        },
        shareStatus: {
            type: DataTypes.STRING,
            comment: '今日是否被邀请,1，已被邀请，2或者空，未被邀请'
        },
        openid: {
            type: DataTypes.STRING,
            comment: '用户openid',
            allowNull: true
        },
        message: {
            type: DataTypes.STRING,
            comment: '消息状态，1发送，2不发送',
            allowNull: true,
        },
        liulan: {
            type: DataTypes.STRING,
            comment: '浏览任务，1已完成，2未完成',
            allowNull: true,
        },
        huodongmiaoshu: {
            type: DataTypes.STRING,
            comment: '优质用户描述信息，1的话代表不是优质用户，2代表之前是优质用户',
            allowNull: true,
        },
        yaoqingUserId: {
            type: DataTypes.STRING,
            comment: '邀请用户id，也就是谁邀请过来的',
            allowNull: true,
        },
        todayYaoqing: {
            type: DataTypes.BOOLEAN,
            comment: '该用户今日是否有邀请新用户',
            defaultValue: false
        },
        luyangmao: {
            type: DataTypes.BOOLEAN,
            comment: '是否是纯路羊毛用户',
            defaultValue: false
        }
    }),
    // 操作失败
    operationFail(res) {
        res.json({
            code: 400,
            msg: '操作失败'
        });
    },
    // 抽奖
    updateMangheUser(req, res, next) {
        let query = req.body;
        let {id, userId, newNum, avatarUrl, index, mangheShareUserId, nickName} = query;
        user.updateUserInfo(nickName, avatarUrl, userId);
        console.log(avatarUrl, userId, index + '用户信息');
        manghe.updateManghe(id, avatarUrl, userId, index, '', (status) => {
            if (status == 300) {
                res.send({
                    code: 300,
                    msg: '已被其他用户开奖',
                    data: {
                        id
                    }
                });
                return;
            }
            if (!status) {
                this.operationFail(res);
                return;
            }
            // 查询信息
            this.mangheUser.findAll({
                where: {
                    userId
                },
                raw: true
            })
            .then(result => {
                let num = result[0].num;
                let oldShareStatus = result[0].shareStatus;
                let orderNumber = result[0].orderNumber;
                let raffleNum = result[0].raffleNum || 0;
                let todayYaoqing = result[0].todayYaoqing;
                let yaoqingUserId = result[0].yaoqingUserId;
                let luyangmao = result[0].luyangmao;
                raffleNum = +raffleNum + 1;
                console.log(!todayYaoqing, todayYaoqing)
                console.log(raffleNum, newNum);
                // 如果是首次抽奖，必中1元
                if (raffleNum ==1) {
                    newNum = 1;
                } else if (!todayYaoqing && raffleNum > 5 && newNum > 0.5) {
                    console.log('走这里逻辑了')
                    // 今天没有邀请新用户，抽奖总次数大于5，并且当前的抽奖中奖金额大于0.5，那么该用户最多只能中0.5元
                    newNum = 0.5;
                }
                else if (luyangmao) {
                    // 如果是纯粹路羊毛用户，只能中0.1元
                    console.log('路羊毛用户')
                    newNum = 0.1;
                }

                let sumNum = (num * 10 + newNum * 10) / 10;
                // 抽奖次数减一
                if (orderNumber > 0) {
                    orderNumber = orderNumber - 1;
                }
                let shareStatus = 2;
                // 更新被分享状态
                if (mangheShareUserId && mangheShareUserId !== 'undefined') {
                    shareStatus = 1;
                }
                // 更新盲盒信息
                this.mangheUser.update(
                    {
                        num: sumNum,
                        orderNumber,
                        shareStatus,
                        raffleNum
                    },
                    {
                        where: {
                            userId
                        }
                    })
                    .then(result => {
                        // 存在分享ID并且未被邀请过，邀请人次数加一
                        if(mangheShareUserId && mangheShareUserId!=='undefined' && oldShareStatus == 2 && mangheShareUserId != userId) {
                            // 更新用户受邀列表
                            mangheInvitationUser.updateUser(mangheShareUserId, userId);
                            this.addOrderNumber(mangheShareUserId, 1);
                        }
                        // 优质用户推荐过来的用户，更新邀请人的金额
                        if (yaoqingUserId) {
                            this.updateYaoqingyonghu(yaoqingUserId, newNum);
                        }
                        manghe.updateManghe(id, avatarUrl, userId, index, newNum, () => {})
                        res.send({
                            code: 200,
                            msg: '操作成功',
                            data: {
                                sumNum,
                                newNum
                            }
                        });
                    })
                    .catch(err => {
                        console.log('更新失败updateMangheUser：', err);
                        this.operationFail(res, err);
                    })
            })
            .catch(err => {
                this.operationFail(res, err);
                console.log('更新失败updateMangheUser：', err);
            });
        });
    },
    // 如果是优质用户，受邀用户抽奖获得的钱，将会抽取50%返利给邀请用户
    // userId 邀请用户的id，newNum 被邀请人当前抽中的奖金
    updateYaoqingyonghu(userId, newNum) {
        this.mangheUser.findAll({
            where: {
                userId
            },
            raw: true
        })
        .then(result => {
            let huodongmiaoshu = result[0].huodongmiaoshu;
            let num = result[0].num;
            let sumNum = (num * 10 + newNum / 2 * 10) / 10;
            if (huodongmiaoshu != 1) {
                this.mangheUser.update(
                    {
                        num: sumNum
                    },
                    {
                        where: {
                            userId
                        }
                    })
                    .then(res => {
                    })
                    .catch(err => {
                        console.log('更新邀请人金额失败updateYaoqingyonghu:', err);
                    });
            }
        })
        .catch(err => {
            console.log('更新邀请人金额失败updateYaoqingyonghu:', err);
        })
    },
    /**
     * 查询所有已提取的用户
     */
    getuseNum() {
        let sqlStr = `select user.id, user.name as name, mangheUser.useNum as useNum from mangheUser  left JOIN user on user.id = mangheUser.userId  where useNum > 0; `
        return sequelize.query(sqlStr, {type: sequelize.QueryTypes.SELECT})
        .then(result => {
            return result;
        })
        .catch(err => {
            console.log('查询已提取用户失败');
            return '';
        });
    },
    // 解决邀请用户默认是undefined的问题，后续小程序发版以后就可以删除掉这部分逻辑了
    updateMangheYaoqingUsrId(mangheShareUserId, id) {
        this.mangheUser.update(
            {
                yaoqingUserId: mangheShareUserId
            },
            {
                where: {
                    id
                }
            })
            .then()
            .catch();
    },
    getMangheUser(req, res, next) {
        const {userId, mangheShareUserId} = req.query;
        console.log('参数中的mangheShareUserId', mangheShareUserId);
        mangheShareUserId == 'undefined' ? '' : mangheShareUserId;
        console.log('mangheShareUserId', mangheShareUserId);
        this.mangheUser.findAll({
            where: {
                userId
            },
            raw: true
        })
        .then( async (result) => {
            if (result && result[0]) {
                // 解决邀请用户，默认是undefind的问题
                if (result[0].yaoqingUserId == 'undefined' && mangheShareUserId) {
                    this.updateMangheYaoqingUsrId(mangheShareUserId, result[0].id);
                }
                let useNumList = await this.getuseNum();
                let jiashuju = [{
                    "name":"一叶十三刺",
                    "useNum":"164"
                },
                {
                    "name":"单雨徒",
                    "useNum":"9.3"
                },
                {
                    "name":"没有你我更好",
                    "useNum":"33.2"
                },
                {
                    "name":"六月情怀",
                    "useNum":"98.1"
                },
                {
                    "name":"往事埋风中",
                    "useNum":"64.8"
                },
                {
                    "name":"十鸦",
                    "useNum":"104"
                },
                {
                    "name":"陌沫",
                    "useNum":"53"
                },
                {
                    "name":"不懂烦人。",
                    "useNum":"97.9"
                },
                {
                    "name":"有你哪都是故乡",
                    "useNum":"10"
                },
                {
                    "name":"顾北",
                    "useNum":"74.2"
                },
                {
                    "name":"走死在岁月里",
                    "useNum":"30.4"
                },
                {
                    "name":"滥情空心",
                    "useNum":"204.3"
                },
                {
                    "name":"川水往事",
                    "useNum":"26.6"
                },
                {
                    "name":"凉秋暖男",
                    "useNum":"74.3"
                },
                {
                    "name":"ぺ约定的爱情つ",
                    "useNum":"84.9"
                },
                {
                    "name":"自我读懂自我",
                    "useNum":"34.6"
                },
                {
                    "name":"兄弟两字不容易",
                    "useNum":"76"
                }];
                result[0].useNumList = [...jiashuju, ...useNumList];
                res.send({
                    code: 200,
                    msg: '操作成功',
                    data: result[0]
                });
            }
            else {
                console.log('传入的参数,', mangheShareUserId);
                this.insertMangheUser(userId, mangheShareUserId);
                res.send({
                    code: 200,
                    msg: '操作成功',
                    data: {
                        num: 0,
                        orderNumber: 1
                    }
                });
            }
        })
        .catch(() => {
            this.operationFail(res);
        });
    },
    // 如果今天用户有邀请新用户，那么todayYaoqing字段变为true
    updatetodayYaoqing(mangheShareUserId) {
        this.mangheUser.update(
            {
                todayYaoqing: 1
            },
            {
                where: {
                    userId: mangheShareUserId
                }
            })
            .then()
            .catch()
    },
    // 创建一条数据
    insertMangheUser(userId, mangheShareUserId) {
        console.log('创建时候的值,', mangheShareUserId);
        if (mangheShareUserId) {
            this.updatetodayYaoqing(mangheShareUserId);
        }
        this.mangheUser.create({
            orderNumber: 1,
            userId,
            num: 0,
            shareStatus: 2,
            liulan: 2,
            yaoqingUserId: mangheShareUserId || '',
            huodongmiaoshu: 1,
        })
        .then()
        .catch()
    },
    // 完成浏览任务增加抽奖次数
    addOrderNumberLiulan(req, res) {
        let {userId} = req.query;
        // 查询信息
        this.mangheUser.findAll({
            where: {
                userId
            },
            raw: true
        })
        .then(result => {
            if (result && result[0]) {
                let orderNumber = result[0].orderNumber;
                    orderNumber = +orderNumber + 1;
                let liulan = result[0].liulan;
                if (liulan == 1) {
                    // 已完成浏览任务，无需新增次数
                    res.send({
                        code: 200,
                        msg: '操作成功',
                        data: false
                    });
                    return;
                }
                // 更新盲盒信息
                this.mangheUser.update(
                    {
                        orderNumber,
                        liulan: 1
                    },
                    {
                        where: {
                            userId
                        }
                    })
                    .then(result => {
                        res.send({
                            code: 200,
                            msg: '操作成功',
                            data: true
                        });
                    })
                    .catch(err => {
                        res.send({
                            code: 400,
                            msg: '操作失败',
                            data: err
                        });
                        console.log('更新失败addOrderNumberLiulan：', err);
                    })
            }
            
        })
        .catch(err => {
            res.send({
                code: 400,
                msg: '操作失败',
                data: 'null'
            });
            console.log('更新失败addOrderNumberLiulan：', err);
        });
    },
    /**
     * 信息审核通过，奖金+2
     * @param {string} userId 用户ID
     */
    addNum(userId) {
        // 查询信息
        this.mangheUser.findAll({
            where: {
                userId
            },
            raw: true
        })
        .then(result => {
            if (result && result[0]) {
                let num = result[0].num;
                num = +num + 2;
                // 更新盲盒信息
                this.mangheUser.update(
                    {
                        num
                    },
                    {
                        where: {
                            userId
                        }
                    })
                .then(result => {
                })
                .catch(err => {
                    console.log('奖金+2失败addNum：', err);
                });
            }
        })
        .catch(err => {
            console.log('奖金+2失败addNum：', err);
        })
    },
    // 抽奖次数增加
    addOrderNumber(userId, addOrderNum, liulanStatus) {
        // 查询信息
        this.mangheUser.findAll({
            where: {
                userId
            },
            raw: true
        })
        .then(result => {
            if (result && result[0]) {
                let orderNumber = result[0].orderNumber;
                    orderNumber = +orderNumber + addOrderNum;
                // 更新盲盒信息
                this.mangheUser.update(
                    {
                        orderNumber
                    },
                    {
                        where: {
                            userId
                        }
                    })
                    .then(result => {
                        // 发送通知
                        // send.sendMangheApi(req.headers['x-wx-openid'], orderNumber);
                        this.sendMangheMessageOne(userId);
                    })
                    .catch(err => {
                        console.log('更新失败addMangheUser：', err);
                    })
            }
            
        })
        .catch(err => {
            console.log('更新失败addMangheUser：', err);
        });
    },
    async sendMangheMessageOne(userId) {
        try {
            let mangheUserInfo = await this.getUserMangheFromUserId(userId);
            let {openid, message, orderNumber} = mangheUserInfo;
            if (openid && message == 1 && orderNumber > 0) {
                send.sendMangheApi(openid, orderNumber);
            }
        }
        catch(err) {
            console.log('sendMangheMessageOne失败', err);
        }
        
    },
    /**
     * 根据用户id查询用户manghe信息
     * @param {*} userId 
     * @returns 
     */
    getUserMangheFromUserId(userId) {
        return this.mangheUser.findAll({
            where: {
                userId
            },
            raw: true
        })
        .then(result =>{
            return result[0];
        })
        .catch(err=> {
            console.log('根据用户ID查询用户manghe信息失败getUserMangheFromUserId',err)
        })
    },
    /**
     * 更新消息通知状态
     */
    updateMangheMessage(req, res) {
        let {userId} = req.query;
        this.mangheUser.update({
            openid: req.headers['x-wx-openid'],
            message: 1
        },
        {
            where: {
                userId
            }
        })
        .then(result => {
            console.log('盲盒消息通知更新完成');
            res.send({
                code: 200,
                msg: '操作成功',
                data: result
            });
        })
        .catch(err => {
            console.log(err);
            res.send({
                code: 200,
                msg: '操作失败',
                data: err
            });
        })
    },
    async updateMangheOpenId(openid) {
        let userInfo = await user.getUserInfoFromOpenId(openid);
    },
    /**
     * 推送盲盒消息
     **/
    messageSends() {
        this.mangheUser.findAll({
            where: {
                message: 1
            },
            raw: true
        })
        .then(result => {
            let datas = result;
            console.log('盲盒列表', datas.length);
            for(let i = 0; i< datas.length; i++) {
                send.sendMangheApi(datas[i].openid, datas[i].orderNumber);
            }
        })
        .catch(err => {
            console.log(err);
        })
    },
    /**
     * 更新盲盒用户次数变更为1，修改分享状态为可分享：2
     */
    updateMangheUserOrderNumberAndShareStatus() {
        this.mangheUser.findAll({
            attributes: ['id'],
            raw: true
        })
        .then(result => {
            console.log(result);
            let arr = [];
            for (let i = 0; i< result.length; i++) {
                arr.push(result[i].id);
            };
            let ids = arr.join(',');
            let sqlStr = `UPDATE mangheUser SET orderNumber='1', shareStatus='2', liulan='2', todayYaoqing=0 WHERE id in (${ids}); `;
            console.log(sqlStr);
            sequelize.query(sqlStr, {type: sequelize.QueryTypes.SELECT})
            .then(results => {
                // results.map is not a function
                console.log('更新次数成功:updateMangheUserOrderNumberAndShareStatus');
                this.messageSends();
            })
            .catch(err => {
                this.messageSends();
                console.log('更新次数失败:updateMangheUserOrderNumberAndShareStatus--updateSql', err);
            })
        })
        .catch(err => {
            console.log('更新次数失败:updateMangheUserOrderNumberAndShareStatus', err);
        })
    },
    /**
     * 提现接口
     */
    tixian(req, res) {
        let {id} = req.query;
        let userId = '141' + id
        this.mangheUser.findAll({
            where: {
                userId
            },
            raw: true
        })
        .then(result => {
            let {num, useNum} = result[0];
            if (num >= 2) {
                this.mangheUser.update(
                    {
                        num: 0,
                        useNum: (num * 10 + useNum * 10) / 10
                    },
                    {
                        where: {
                            userId
                        }
                    })
                    .then(result1 => {
                        res.send({
                            code: 200,
                            data: result
                        });
                    })
                    .catch(err => {
                        res.send({
                            code: 400,
                            data: err
                        });
                    })
            }
            else {
                res.send({
                    code: 400,
                    data: '当前用户余额为' + num + '不能完成提现'
                });
            }
            
        })
        .catch(err => {
            res.send({
                code: 400,
                data: err
            });
        })
    },
    setjinyong() {
        this.mangheUser.findAll({
            attributes: ['id']
        })
        .then(result => {
            let arr= result.map(res => {
                return res.id
            });
            // console.log(arr);
            let str = arr.join(',');
            let sqlStr = `update mangheUser set huodongmiaoshu='因疫情影响，抽奖活动暂停一周，已满两元的可以先提现，未满两元的下周活动开启后可以继续抽奖' where id in (${str})`;
            // console.log(sql)
            sequelize.query(sqlStr, {type: sequelize.QueryTypes.SELECT})
        })
    }
}