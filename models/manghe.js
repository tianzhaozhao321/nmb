/**
 * 活动模型
 */

 const {defineModel, sequelize} = require('../db');
 const {DataTypes, Op } = require("sequelize");
 const user = require('./user.js');
 
module.exports = {
    manghe: defineModel('manghe', {
        info: {
            type: DataTypes.TEXT('medium'),
            comment: '活动数据'
        }
    }),
    // 操作失败
    operationFail(res) {
        res.json({
            code: 400,
            msg: '操作失败'
        });
    },
    /**
     * 更新盲盒
     */
    updateManghe(id, avatarUrl, userId, index, newNum, cb) {
        console.log('updateManghe', newNum);
        // 查询信息
        this.manghe.findAll({
            where: {
                id
            },
            raw: true
        })
        .then(result => {
            let info = JSON.parse(result[0].info);
            // 这块有个坑，如果是已选择的用户，新用户有可能会把它给覆盖掉，后续再优化吧
            if(info && info[index] && info[index].userId && !newNum) {
                cb(300);
                return;
            }
            info[index].avatarUrl = avatarUrl;
            info[index].userId = userId;
            console.log('更新num', newNum);
            if (newNum) {
                console.log('执行了更新num');
                info[index].num = newNum;
            }
            // 更新盲盒信息
            this.manghe.update(
                {
                    info: JSON.stringify(info)
                },
                {
                    where: {
                        id
                    }
                })
                .then(result => {
                    cb(true);
                })
                .catch(err => {
                    cb(false);
                    // this.operationFail(res);
                    console.log('更新失败updateManghe：', err);
                })
        })
        .catch(err => {
            cb(false);
            console.log('更新失败updateManghe：', err);
        });
    },
    /**
     * 获取盲盒信息
     */
    // getManghe(req, res, next) {
    //     this.manghe.findAll({
    //         where: {
    //             id: 1
    //         },
    //         raw: true
    //     })
    //     .then(result => {
    //         // console.log(result)
    //         result[0].info = JSON.parse(result[0].info);
    //         // console.log('result[0].info', result[0].info)
    //         res.send({
    //             code: 200,
    //             msg: '操作成功',
    //             data: result[0]
    //         });
    //     })
    //     .catch(err => {
    //         console.log('获取盲盒信息失败：getManghe', err);
    //         this.operationFail(res);
    //     });
    // },
    createManghe(req, res) {
        // 生成随机数组
        function getRandom(maximum) {
            let minimum = 1;
            // let num = Math.floor(Math.random() * (maximum - minimum + 1)) + minimum;
            let num = Math.random()*(minimum-maximum)+maximum;
            return Math.floor(num);
        }
        let arr = []
        for (let i = 0; i < 130; i++) {
            arr.push({
                num: 0.1
            });
        }
        for (let i = 0; i < 50; i++) {
            let randm = getRandom(130);
            // console.log(randm)
            arr[randm].num = 0.5;
        }

        for (let i = 0; i < 20; i++) {
            arr[getRandom(130)].num = 1;
        }

        for (let i = 0; i < 7; i++) {
            arr[getRandom(130)].num = 2;
        }

        for (let i = 0; i < 3; i++) {
            // console.log(getRandom(30))
            arr[getRandom(130)].num = 5;
        }
        let allNum = 0;
        for (let i = 0; i< arr.length; i++) {
            allNum = (allNum * 10 + arr[i].num * 10) / 10
        }
        console.log(allNum);
        this.manghe.create({
            info: JSON.stringify(arr)
        })
        .then(result => {
            res.send({
                code: 200,
                msg: '操作成功',
                data: arr
            });
        })
        .catch(err => {
            console.log('manghe数据创建失败', err);
            res.send({
                code: 400,
                msg: '操作失败',
                data: err
            });
        })
    },
    // 获取最后一条数据
    getManghe(req, res) {
        // 文档 https://www.jianshu.com/p/aa0063c80249
        this.manghe.findAll({
            attributes: ['info', 'id'],
            order: [
                ['id', 'DESC']
            ],
            limit:1
        })
        .then(result => {
            result[0].info = JSON.parse(result[0].info);
            res.send({
                code: 200,
                msg: '操作成功',
                data: result[0]
            });
        })
        .catch(err => {
            console.log('获取盲盒信息失败：getManghe', err);
            this.operationFail(err);
        });
    }
}