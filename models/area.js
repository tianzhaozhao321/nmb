/**
 * 地区模型
 */
/**
 * 城市模型
 */

const {defineModel, sequelize} = require('../db');
const {DataTypes} = require("sequelize");
module.exports = {
    area: defineModel('recruitment', {
        AREA_CODE: {
            type: DataTypes.STRING,
            comment: '区代码'
        },
        AREA_NAME: {
            type: DataTypes.STRING,
            comment: '区名称'
        },
        SHORT_NAME: {
            type: DataTypes.STRING,
            comment: '简称'
        },
        CITY_CODE: {
            type: DataTypes.STRING,
            comment: '父级市代码'
        },
        LNG: {
            type: DataTypes.STRING,
            comment: '精度'
        },
        LAT: {
            type: DataTypes.STRING,
            comment: '纬度'
        }
    }),
    /**
     * 根据地区获取省市编码
     */
    getLocation(req, res, next) {
        let query = req.query;
        const {adcode} = query;
        let sql = `select PROVINCE_CODE as provinceCode, CITY_CODE as cityCode from city where CITY_CODE in (select CITY_CODE from area where AREA_CODE=${adcode})`;
        // req.query.
        sequelize.query(sql, { type: sequelize.QueryTypes.SELECT })
            .then(result => {
                res.json({
                    code: 200,
                    msg: '操作成功',
                    data: result[0]
                });
            })
            .catch(err => {
                console.log('根据地区获取省市编码失败getLocation:', err);
                res.json({
                    code: 400,
                    msg: '操作失败',
                    data: err
                });
            });
    },
};
 
// CREATE TABLE `area` (
//     `AREA_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增列',
//     `AREA_CODE` varchar(40) NOT NULL COMMENT '区代码',
//     `CITY_CODE` varchar(40) DEFAULT NULL COMMENT '父级市代码',
//     `AREA_NAME` varchar(40) NOT NULL COMMENT '市名称',
//     `SHORT_NAME` varchar(20) NOT NULL COMMENT '简称',
//     `LNG` varchar(20) DEFAULT NULL COMMENT '经度',
//     `LAT` varchar(20) DEFAULT NULL COMMENT '纬度',
//     `SORT` int(6) DEFAULT NULL COMMENT '排序',
//     `GMT_CREATE` datetime DEFAULT NULL COMMENT '创建时间',
//     `GMT_MODIFIED` datetime DEFAULT NULL COMMENT '修改时间',
//     `MEMO` varchar(250) DEFAULT NULL COMMENT '备注',
//     `DATA_STATE` int(11) DEFAULT NULL COMMENT '状态',
//     `TENANT_CODE` varchar(32) DEFAULT NULL COMMENT '租户ID',
//     PRIMARY KEY (`AREA_ID`),
//     KEY `Index_1` (`AREA_CODE`,`TENANT_CODE`)
//   ) ENGINE=InnoDB AUTO_INCREMENT=3679 DEFAULT CHARSET=utf8 COMMENT='地区设置';