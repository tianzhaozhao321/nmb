const {defineModel, sequelize} = require('../db');
const {DataTypes } = require("sequelize");
const {timeString} = require('../util/util.js');
const moment = require('moment');
const user = require('./user.js');

module.exports = {
    conllection: defineModel('conllection', {
        content: {
            type: DataTypes.TEXT,
            comment: '收藏的信息'
        },
        userId: {
            type: DataTypes.INTEGER,
            comment: '用户ID'
        },
        contentId: {
            type: DataTypes.STRING,
            comment: '收藏来源内容的id'
        },
        type: {
            type:  DataTypes.STRING,
            comment: '来源，1：招工，2：找活，3：跳蚤市场，4：商家'
        },
        date: {
            type:  DataTypes.STRING,
            comment: '收藏时间',
            get() {
                return moment(this.getDataValue('create_time')).format('YYYY-MM-DD HH:mm:ss');
            }
        }
    }),
    /**
     * 添加收藏
     */
    insertConllection(req, res, next) {
        let query = req.body;
        const {content, userId, contentId, type} = query;
        let obj = {
            content,
            userId,
            contentId,
            type,
            date: timeString()
        };
        this.conllection.create(obj)
            .then(result => {
                res.send({
                    code: 200,
                    msg: '操作成功',
                    data: {
                        id: result.dataValues.id
                    }
                });
            })
            .catch(err => {
                res.json({
                    code: 400,
                    msg: '新增失败'
                });
                console.log('收藏失败insertConllection:', err);
            });
    },
    /**
     * 获取收藏列表
     */
    getConllectionList(req, res, next) {
        let {page, pageSize = 15, userId} = req.query;
        let limitStar = page * pageSize - pageSize;
        this.conllection.findAll({
            where: {
                userId
            },
            order: [['date', 'desc']],
            offset: limitStar || 0,
            limit: +pageSize,
            raw: true
        })
            .then(result => {
                res.send({
                    code: 200,
                    msg: '操作成功',
                    data: result
                });
            })
            .catch(err => {
                console.log('获取收藏列表失败getConllectionList：', err);
                res.send({
                    code: 400,
                    msg: '操作失败',
                    data: err
                });
            });
    },

    /**
     * 获取收藏详情
     */
    getConllectionDetai(req, res, next) {
        let {id, userId, contentId, type} = req.query;
        let where = {};
        if (id) {
            where = {
                id
            };
        }
        else {
            where = {
                userId,
                contentId,
                type
            };
        }
        this.conllection.findAll({
            where: {
                ...where
            }
        })
            .then(result => {
                res.send({
                    code: 200,
                    msg: '操作成功',
                    data: result
                });
            })
            .catch(err => {
                console.log('获取收藏详情失败getConllectionDetai:', err);
                res.send({
                    code: 400,
                    msg: '操作失败',
                    data: err
                });
            });
    },
    /**
     * 取消收藏
     */
    canCelConllection(req, res, next) {
        let {id, userId, contentId, type} = req.query;
        let where = {};
        if (id) {
            where = {
                id
            };
        }
        else {
            where = {
                userId,
                contentId,
                type
            };
        }
        this.conllection.destroy({
            where: {
                ...where
            }
        })
            .then(result => {
                res.send({
                    code: 200,
                    msg: '操作成功',
                    data: result
                });
            })
            .catch(err => {
                console.log('取消收藏getConllectionDetai:', err);
                res.send({
                    code: 400,
                    msg: '操作失败',
                    data: err
                });
            });
    },

    /**
     * 收藏总数
     */
    conllectionCount(userId) {
        return this.conllection.count({
            where: {
                userId
            }
        })
            .then(result => {
                return result;
            })
            .catch(err => {
                console.log('获取收藏总数失败conllectionCount', err);
            })
    }
};
