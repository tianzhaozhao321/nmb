/**
 * 省模型
 */
const {defineModel, sequelize} = require('../db');
const {DataTypes } = require("sequelize");
module.exports = {
    province: defineModel('recruitment', {
        PROVINCE_CODE: {
            type: DataTypes.STRING,
            comment: '省份代码'
        },
        PROVINCE_NAME: {
            type: DataTypes.STRING,
            comment: '省份名称'
        },
        SHORT_NAME: {
            type:  DataTypes.STRING,
            comment: '简称'
        },
        LNG: {
            type:  DataTypes.STRING,
            comment: '精度'
        },
        LAT: {
            type:  DataTypes.STRING,
            comment: '纬度'
        }
    })
};
// CREATE TABLE `province` (
//     `PROVINCE_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增列',
//     `PROVINCE_CODE` varchar(40) NOT NULL COMMENT '省份代码',
//     `PROVINCE_NAME` varchar(50) NOT NULL COMMENT '省份名称',
//     `SHORT_NAME` varchar(20) NOT NULL COMMENT '简称',
//     `LNG` varchar(20) DEFAULT NULL COMMENT '经度',
//     `LAT` varchar(20) DEFAULT NULL COMMENT '纬度',
//     `SORT` int(6) DEFAULT NULL COMMENT '排序',
//     `GMT_CREATE` datetime DEFAULT NULL COMMENT '创建时间',
//     `GMT_MODIFIED` datetime DEFAULT NULL COMMENT '修改时间',
//     `MEMO` varchar(250) DEFAULT NULL COMMENT '备注',
//     `DATA_STATE` int(11) DEFAULT NULL COMMENT '状态',
//     `TENANT_CODE` varchar(32) DEFAULT NULL COMMENT '租户ID',
//     PRIMARY KEY (`PROVINCE_ID`),
//     KEY `Index_1` (`PROVINCE_CODE`,`TENANT_CODE`)
//   ) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8 COMMENT='省份设置';