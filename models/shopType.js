/**
 * 店铺类型模型
 */
// CREATE TABLE `shop_type` (
// `id` int(11) NOT NULL,
// `name` varchar(255) NOT NULL,
// UNIQUE KEY `id` (`id`)
// ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
const db = require('../db');
const {DataTypes } = require("sequelize");
module.exports = {
    shopType: db.defineModel('shopType', {
        name: {
            type: DataTypes.STRING,
            allowNull: true,
            comment: '店铺类型的名称'
        }
    }),
    // 商品类型
    getShopType(req, res, next) {
        this.shopType.findAll()
            .then(result => {
                res.send({
                    code: 200,
                    msg: '操作成功',
                    data: result
                });
            })
            .catch(err => {
                console.log('获取商品类型失败getShopType：', err);
                res.json({
                    code: 400,
                    msg: '操作失败',
                    data: err
                })
            });
    },
};
