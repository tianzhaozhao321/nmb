/**
 * 定时任务
 * 文档：https://juejin.cn/post/7030078229792686116
 */
 const mangheUser = require('../models/mangheUser.js');
const recruitment = require('../models/recruitment.js');
const schedule = require('node-schedule');

class Interval {
    constructor({ unit_name, maintain_time, last_alarm }) {
        this.unit_name = unit_name          // 任务名字
        this.maintain_time = maintain_time  // 定时时间
        this.last_alarm = last_alarm || ""        // 上一次定时任务名字
    }

    // 生成新的定时任务
    async create(callback) {
        // 终止之前的定时任务
        if (this.last_alarm !== "") {
            this.delete(this.last_alarm)
        }
        schedule.scheduleJob(`${this.unit_name}`, `${this.maintain_time}`, callback);
    }

    // 删除定时任务
    delete() {
        if (schedule.scheduledJobs[this.unit_name]) {
            schedule.scheduledJobs[this.unit_name].cancel();
            return true
        }
            return false
    }

    // 找到一个定时任务
    findOne(name) {
        if (schedule.scheduledJobs[name]) {
            return schedule.scheduledJobs[name]
        } else {
            throw new Error("未找到任务名")
        }
    }

    // 查看所有的定时任务
    findAll() {
        return schedule.scheduledJobs
    }
};
 

// 时区的问题，所以需要减去8个小时
// new Interval({
//     unit_name: '每天凌晨24点更新抽盲盒次数',
//     maintain_time: '0 0 16 * * *',
//     last_alarm: '每天凌晨更新抽盲盒次数'
// }).create(async () => {
//     console.log('24点开始更新抽奖次数');
//     mangheUser.updateMangheUserOrderNumberAndShareStatus();
    
// });

new Interval({
    unit_name: '取消置顶',
    maintain_time: '30 1 * * * *',
    last_alarm: '取消置顶'
}).create(async () => {
    console.log('每小时的1分30秒触发');
    recruitment.cancelZhiding();
});

