const multer = require("multer");
const path = require("path");
const fs = require('fs');
const images = require('images');
// 图片压缩
// console.log(path.join(path.dirname(__dirname), '../public', 'img'))
const uploader = multer({
    dest: path.join(path.dirname(__dirname), '../public', 'img')
});

module.exports = {
    uploader: uploader.single('photoImg'),
    // 存储图片
    uploaderSave(req, res) {
        let file = req.file;
        const extname = path.extname(file.originalname);
        const filePath = `${file.path}`;
        const fileName = file.filename + extname;
        console.log(filePath);
        console.log(fileName);
        console.log(path.join(path.dirname(filePath)));
        fs.rename(filePath, path.join(path.dirname('/app' + filePath), fileName), async err => {
            if (!err) {
                res.send({
                    path: `https://express-ifqt-1773259-1255525351.ap-shanghai.run.tcloudbase.com/static/img/${fileName}`
                });
            }
            else {
                res.send({
                    msg: 'upload image fail'
                });
                console.log('上传图片失败', err);
            }
        });
        // images(filePath)
        // .save(path.join(path.dirname('/app' + filePath), fileName), {
        //     quality: 50
        // });
        // res.send({
        //     path: `https://express-ifqt-1773259-1255525351.ap-shanghai.run.tcloudbase.com/static/img/${fileName}`
        // });
    }
};
