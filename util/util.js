var dayjs = require('dayjs');
var utc = require('dayjs/plugin/utc');
var timezone = require('dayjs/plugin/timezone');
dayjs.extend(utc);
dayjs.extend(timezone);
console.log(dayjs('2022-11-12 13:59').unix())
console.log(new Date('2022-11-12 13:59').valueOf());
/**
 * 获取时间戳
 *
 * @return {string} 返回时间戳
 */
const timeString = () => {
    return (new Date()).valueOf();
}
const timeFormat = () => {
    // 时区https://www.bilibili.com/video/BV1NP411j7Ee/?vd_source=e4e4a967d3325378de4c520049527262
    return dayjs().tz("PRC").format('YYYY-MM-DD HH:mm:ss');
}
// 获取时间戳，如果没有参数就是当前时间戳
const timeStr = (timer='') => {
    if (timer) {
        return dayjs(timer).unix();
    }
    return dayjs().tz("PRC").unix();
}

// 前一天的时间戳
const timeFormatPreData = () => {
    return dayjs((new Date()).valueOf()-86400000).tz("PRC").format('YYYY-MM-DD HH:mm:ss');
};

// 精确到分的格式化时间
const timeFormatM = () => {
    // 时区https://www.bilibili.com/video/BV1NP411j7Ee/?vd_source=e4e4a967d3325378de4c520049527262
    return dayjs().tz("PRC").format('YYYY-MM-DD HH:mm');
}

// console.log(timeFormatM(+'1666580065024'));
// console.log(timeFormat())

const customWhereSql = query => {
    let {provinceCode, cityCode, areaCode} = query;
    let where = 'where examineStatus=3';
    if (provinceCode) {
        where += ` and provinceCode='${provinceCode}'`;
    }
    if (cityCode && cityCode != '000000') {
        where += ` and cityCode='${cityCode}'`;
    }
    if (cityCode && areaCode != '000000') {
        where += ` and adcode='${areaCode}'`;
    }
    return where;
};

const customWhere = query => {
    let {provinceCode, cityCode, areaCode} = query;
    let where = {};
    if (provinceCode) {
        where = {
            provinceCode
        }
    }
    if (cityCode && cityCode != '000000') {
        where = {
            cityCode
        }
    }
    if (areaCode && areaCode != '000000') {
        where = {
            adcode: areaCode
        }
    }
    return where;
}

// 格式化时间，大于7天返回false
const formatTimer = (timer) => {
    let now = timeStr() + 8 * 60 * 60;
    let time = timeStr(timer);
    let differ = now - time;
    // 小于一小时，以分钟计算
    let hour = 60 * 60;
    if (differ < hour) {
        return Math.floor(differ / 60) + '分钟前';
    }
    // 小于24小时，以小时记
    let hour24 = 24 * 60 * 60;
    if (differ < hour24) {
        return Math.floor(differ / (60 * 60)) + '小时前';
    }
    // 小于7天，以天计算
    let day7 = 7 * 24 * 60 * 60;
    if (differ < day7) {
        return Math.floor(differ / (24 * 60 * 60)) + '天前'
    }
    return false;
}

module.exports = {
    timeString,
    customWhere,
    customWhereSql,
    timeFormat,
    timeFormatPreData,
    timeFormatM,
    timeStr,
    formatTimer
}