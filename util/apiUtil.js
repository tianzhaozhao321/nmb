/**
 * 添加店铺相关
 */
const {sequelize} = require('../db');
var fs = require('fs');
module.exports = {
    /**
     * 
     * @param {string} adcode 县级编码
     */
    getProvinceCityCode(adcode) {
        return sequelize.query(`select PROVINCE_CODE as provinceCode, CITY_CODE as cityCode from city where CITY_CODE = (select CITY_CODE FROM area where AREA_CODE = ${adcode})`, { type: sequelize.QueryTypes.SELECT })
        .then(data => {
            return data && data[0] || {};
        }).catch(err => {
            console.log('获取城市和身份编码错误：getProvinceCityCode：', err)
        });
    },
    setAreaToFile: function (req, res, next) {
        sequelize.query(`select PROVINCE_CODE as code, PROVINCE_NAME as name  from province`, { type: sequelize.QueryTypes.SELECT })
        .then(result => {
            result.forEach((val, index) => {
                sequelize.query(`select CITY_CODE as code, CITY_NAME as name, PROVINCE_CODE as preCode  from city where PROVINCE_CODE=${val.code}`, { type: sequelize.QueryTypes.SELECT })
                    .then(cityResult => {
                        let cityTop = [{
                            code: '000000',
                            name: '全省',
                            preCode: cityResult[0].preCode,
                            area: [{
                                code: '000000',
                                name: '全市'
                            }]
                        }];
                        val.city = cityTop.concat(cityResult);
                        // val.city = cityResult;
                        cityResult.forEach((val1, index1) => {
                            sequelize.query(`select AREA_CODE as code, AREA_NAME as name  from area where CITY_CODE=${val1.code}`, { type: sequelize.QueryTypes.SELECT })
                            .then(areaResult => {
                                let areaTop = [{
                                    code: '000000',
                                    name: '全市'
                                }]
                                val1.area = areaTop.concat(areaResult);
                                if (index1 === (cityResult && cityResult.length - 1) && index === result.length - 1) {
                                    // jsonWrite(res, result);
                                    let str = JSON.stringify(result);
                                    // res.json(result);
                                    fs.writeFile(__dirname + '/data.json', str, function (err) {
                                        if (err) { console.log(err) }
                                        else {
                                            res.json({
                                                code: '1',
                                                msg: '操作成功'
                                            });
                                        }
                                    });
                                }
                            })
                            .catch(err => {
                                console.log('地区查询失败setAreaToFile', err);
                            });
                        });
                    })
                    .catch(err => {
                        console.log('城市查询失败setAreaToFile', err);
                    })
            });
        })
        .catch(err => {
            console.log('省查询失败setAreaToFile', err);
        })
    },
    // 获取电话号码
    getPhoneNumber(req, res, next) {
        let query = req.query;
        const {id, type, userId} = query;
        // 1：招工，2：找活，3：跳蚤市场，4：商家
        let tableArr = [
            '',
            'recruitment',
            'findJob',
            'market',
            'shop',
        ];
        let zd = [
            '',
            'workDescribe as title',
            'introduce as title',
            'marketDescribe as title',
            'showName as title'
        ]
        sequelize.query(`select phone, ${zd[type]} from ${tableArr[type]} where id = ${id}`, { type: sequelize.QueryTypes.SELECT })
            .then(result => {
                // if (type != 1 && type !=2){
                    res.send({
                        code: 200,
                        msg: '操作成功',
                        data: result
                    });
                // }
                // else {
                //     sequelize.query(`update user set integral = integral-1 where id = ${userId}`)
                //     .then(result1 => {
                //         res.send({
                //             code: 200,
                //             msg: '操作成功',
                //             data: result
                //         });
                //     })
                //     .catch(err => {
                //         console.log('更新用户积分失败:getPhoneNumber：', err);
                //         res.send({
                //             code: 400,
                //             msg: '操作失败',
                //             data: err
                //         });
                //     });
                // }
                let telPhone = require('../models/telPhone.js');
                telPhone.insertTelPhone({
                    content: result[0].title,
                    userId,
                    contentId: id,
                    type
                });
            })
            .catch(err => {
                console.log('获取电话号码失败getPhoneNumber：', err);
                res.send({
                    code: 400,
                    msg: '操作失败',
                    data: err
                });
            });
    },
    // 地区列表
    selectArea(req, res, next) {
        fs.readFile(__dirname + '/data.json', 'utf-8', function (err, data) {
            if (err) {
                console.log(err);
            } else {
                res.send(JSON.parse(data));
            }
        });
    },
    // 地区列表-实际
    getAreaList(req, res) {
        let arr = [
            {
                "name": "全部地区",
                "code": "111111"
            },
            {
                "name": "晋州",
                "code": "130183"
            },
            {
                "name": "辛集",
                "code": "130181"
            },
            {
                "name": "赞皇",
                "code": "130129"
            },
            {
                "name": "其他地区",
                "code": "222222"
            }
        ];
        res.send({
            code: 200,
            msg: '操作成功',
            data: arr
        });
    },
    // 获取分类列表-实际
    getTypeList(req, res){
        let arr = [
            {
                "name": "全部分类",
                "code": "111111"
            },
            {
                "name": "招工",
                "code": "1"
            },
            {
                "name": "找活",
                "code": "2"
            },
            {
                "name": "保洁",
                "code": "3"
            },
            {
                "name": "求购",
                "code": "4"
            },
            {
                "name": "二手",
                "code": "11"
            },
            {
                "name": "出售",
                "code": "5"
            },
            {
                "name": "搬家",
                "code": "6"
            },
            {
                "name": "求租",
                "code": "12"
            },
            {
                "name": "出租",
                "code": "7"
            },
            {
                "name": "家政",
                "code": "8"
            },
            {
                "name": "本地农产品",
                "code": "9"
            },
            {
                "name": "美食店铺",
                "code": "10"
            },
            {
                "name": "其他",
                "code": "1001"
            }
        ];
        console.log('获取接口结束', Date.now())
        res.send({
            code: 200,
            msg: '操作成功',
            data: arr
        });
    }
}