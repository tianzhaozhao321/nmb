const path = require('path');
const express = require('express');
const cors = require('cors');
const morgan = require('morgan');
const {setAreaToFile, selectArea, getPhoneNumber, getAreaList, getTypeList} = require('./util/apiUtil.js');
const uploadImg = require('./util/uploadImg.js');
// const {init: initDB, Counter} = require('./db');
const scheduler = require('./util/scheduler.js');

const {
    recruitment,
    user,
    workType,
    city,
    province,
    area,
    home,
    findJob,
    market,
    transactionType,
    shopType,
    shop,
    conllection,
    shenhe,
    telPhone,
    activity,
    activityUser,
    hot,
    scanCode,
    send,
    qiandao,
    manghe,
    mangheUser,
    zhaoPinStatus,
    mangheInvitationUser,
    recruitmentUserFanKui,
    searchName
} = require('./model.js');

const logger = morgan('tiny');

const app = express();
console.log(path.join(__dirname, 'public'));
app.use('/static', express.static(path.join(__dirname, 'public')));
app.use(express.urlencoded({extended: false}));
app.use(express.json());
app.use(cors());
app.use(logger);

// // 首页
app.get('/', async (req, res) => {
    res.sendFile(path.join(__dirname, 'index.html'));
});

// 获取电话号码
app.get('/info/getPhoneNumber', (req, res, next) => {
    getPhoneNumber(req,res,next);
});

/**
 * 用户信息相关接口
 */
// 登录注册
app.post('/login', async (req, res, next) => {
    await user.login(req, res, next);
});
// 测试登录注册
app.get('/login1', async (req, res, next) => {
    await user.login1(req, res, next);
});
// app.get('/user', async (req, res, next) => {
//     await user.getUser(req, res, next);
// });
// 获取用户信息
app.get('/getUserInfo', async (req, res, next) => {
    await user.getUserInfoFromId(req, res, next);
});
// 获取用户信息
app.get('/svg', async (req, res, next) => {
    await user.svg(req, res, next);
});
// 更新用户经纬度信息
app.get('/updateLatLong', async (req, res, next) => {
    await user.updateLatLong(req, res, next);
});



// 获取工作类型
app.get('/workType', (req, res, next) => {
    workType.getWorkType(req, res, next);
});
// 提交招工
app.post('/addRecruitment', (req, res, next) => {
    recruitment.insertRecruitment(req, res, next);
});
// // 获取工作类型
app.get('/getRecruitment', (req, res, next) => {
    recruitment.getRecruitment(req, res, next);
});
// // 根据userId获取招工列表
app.get('/getRecruitmentList', (req, res, next) => {
    recruitment.getRecruitmentList(req, res, next);
});
// // 根据userId获取审核列表
app.get('/getRecruitmentListShenhe', (req, res, next) => {
    recruitment.getRecruitmentListShenhe(req, res, next);
});


// // 获取招工列表
app.get('/list/getRecruitmentListAll', (req, res, next) => {
    recruitment.getRecruitmentListAll(req, res, next);
});

// 置顶
app.get('/recruitment/goToping', (req, res, next) => {
    recruitment.goToping(req, res, next);
});
// 刷新
app.get('/recruitment/refresh', (req, res, next) => {
    recruitment.refresh(req, res, next);
});
// 更新关闭操作
app.get('/recruitment/closed', (req, res, next) => {
    recruitment.closed(req, res, next);
});
app.get('/recruitment/updateriqi', (req, res, next) => {
    recruitment.updateriqi(req, res, next);
});
app.get('/cancelZhiding', (req, res, next) => {
    recruitment.cancelZhiding(req, res, next);
});


// 用户反馈信息
app.post('/recruitmentUserFanKui/createRecruitmentUserFanKui', (req, res, next) => {
    recruitmentUserFanKui.insertRecruitmentUserFanKui(req, res, next);
});

// 导入数据-晋州
app.post('/daorushuju', (req, res, next) => {
    recruitment.daorushuju(req, res, next);
});

/**
 * 地区接口
 */
// 生成新的省市区列表
app.get('/setAreaToFile', (req, res, next) => {
    setAreaToFile(req, res, next);
});

// 获取省市区列表
app.get('/getArea', (req, res, next) => {
    selectArea(req, res, next);
});
// 获取真实地区列表 -实际
app.get('/getAreaList', (req, res, next) => {
    getAreaList(req, res, next);
});

// 获取分类列表-实际
app.get('/getTypeList', (req, res, next) => {
    console.log('获取接口开始时间', Date.now())
    getTypeList(req, res, next);
});

// 获取省、市编码
app.get('/getLocation', (req, res, next) => {
    area.getLocation(req, res, next);
});

// 图片上传
app.post('/uploadHead', uploadImg.uploader, (req, res) => {
    uploadImg.uploaderSave(req, res);
});

/**
 * 找活相关接口
 */
// 提交招工
app.post('/addFindJob', (req, res, next) => {
    findJob.insertFindJob(req, res, next);
});

// 根据id查询我的找活
app.get('/getFindJob', (req, res, next) => {
    findJob.getFindJob(req, res, next);
});

// 根据userId查询我的找活
app.get('/getFindJobUser', (req, res, next) => {
    findJob.getFindJobUser(req, res, next);
});

// 根据userid和jobid查找找活详情
app.get('/findJobDetail', (req, res, next) => {
    findJob.findJobDetail(req, res, next);
});

// 获取全部找活列表
app.get('/list/getFindJobList', (req, res, next) => {
    findJob.getFindJobList(req, res, next);
});

/**
 * 商品相关接口
 */
// 添加商品
app.post('/addMarket', (req, res, next) => {
    market.insertMarket(req, res, next);
});

// 获取某用户商品列表
app.get('/getMarketUser', (req, res, next) => {
    market.getMarketUser(req, res, next);
});

// 列表页获取全部商品列表
app.get('/list/getMarketList', (req, res, next) => {
    market.getMarketList(req, res, next);
});

// 获取某用户商品详情
app.get('/getMarketDetail', (req, res, next) => {
    market.getMarket(req, res, next);
});

// 获取商品类型
app.get('/getTransactionType', (req, res, next) => {
    transactionType.getTransactionType(req, res, next);
});


/**
 * 店铺相关接口
 */
// 获取店铺类型
app.get('/getShop', (req, res, next) => {
    shopType.getShopType(req, res, next);
});
// 提交店铺
app.post('/addShop', (req, res, next) => {
    shop.insertShop(req, res, next);
});
// 根据userId获取店铺列表
app.get('/getShopList', (req, res, next) => {
    shop.getShopList(req, res, next);
});
//  首页店铺列表
app.get('/list/getShopListAll', (req, res, next) => {
    shop.getShopListAll(req, res, next);
});
app.get('/getShopDetail', (req, res, next) => {
    shop.getShop(req, res, next);
});

/**
 * 收藏相关接口
 */
// 新增收藏
app.post('/addConllection', (req, res, next) => {
    conllection.insertConllection(req, res, next);
});
// 获取收藏列表
app.get('/getConllectionList', (req, res, next) => {
    conllection.getConllectionList(req, res, next);
});
// 获取收藏详情
app.get('/getConllectionDetail', (req, res, next) => {
    conllection.getConllectionDetai(req, res, next);
});
// 取消收藏
app.get('/canCelConllection', (req, res, next) => {
    conllection.canCelConllection(req, res, next);
});


/**
 * 联系记录相关接口
 */
// 获取联系记录列表
app.get('/getTelPhoneList', (req, res, next) => {
    telPhone.getTelPhoneList(req, res, next);
});
// 获取联系记录详情
app.get('/getTelPhoneDetail', (req, res, next) => {
    telPhone.getTelPhoneDetai(req, res, next);
});

/**
 * 首页接口
 */
app.get('/home/getAllList', (req, res, next) => {
    home.getAllList(req, res, next);
});
/**
 * 首页招工列表
 */
app.get('/home/getRecruitmentList', (req, res, next) => {
    recruitment.getRecruitmentListAll(req, res, next);
});

/**
 * 首页店铺列表
 */
app.get('/home/getShopList', (req, res, next) => {
    shop.getShopListAll(req, res, next);
});

/**
 * 首页二手交易列表
 */
app.get('/home/getErShou', (req, res, next) => {
    market.getErShou(req, res, next);
});

/**
 * 首页跳蚤市场列表
 */
app.get('/home/getMarketList', function(req, res, next){
    market.homeGetMarketList(req, res, next);
});

/**
 * 首页找活列表
 */
app.get('/home/getFindJobList', (req, res, next) => {
    findJob.getFindJobList(req, res, next);
});

/**
 * 审核相关接口
 */
// 获取审核列表
app.get('/shenhe/getAllList', (req, res, next) => {
    shenhe.getAllList(req, res, next);
});
// 提交审核
app.post('/shenhe/addShenhe', (req, res, next) => {
    shenhe.insertShenhe(req, res, next);
});
// 获取审核详情
app.get('/shenhe/getShenheDetail', (req, res, next) => {
    shenhe.getShenheDetail(req, res, next);
});

/**
 * 活动相关接口
 */

// 根据ID查询活动详情
app.get('/activity/getActivity', (req, res, next) => {
    activity.getActivity(req, res, next);
});
// 新增活动
app.post('/activity/addActivity', (req, res, next) => {
    activity.insertActivity(req, res, next);
});
// 根据用户ID查询活动列表
app.get('/activity/getActivityUser', (req, res, next) => {
    activity.getActivityUser(req, res, next);
});
// 根据资源ID查询活动
app.get('/activity/getActivityContentId', (req, res, next) => {
    activity.getActivityContentId(req, res, next);
});

// 根据距离查询活动
app.get('/activity/getActivityJuli', (req, res, next) => {
    activity.getActivityJuli(req, res, next);
});

// 新增用户领取活动
app.post('/activityUser/insertActivityUser', (req, res, next) => {
    activityUser.insertActivityUser(req, res, next);
});
// 获取已选择活动列表
app.get('/activityUser/getActivityUser', (req, res, next) => {
    activityUser.getActivityUser(req, res, next);
});

// 查询是否已领取活动
app.get('/activityUser/getActivityUserStatus', (req, res, next) => {
    activityUser.getActivityUserStatus(req, res, next);
});

// 获取活动使用二维码
app.get('/activityUser/getActivitySvg', (req, res, next) => {
    activityUser.getActivitySvg(req, res, next);
});
// 扫码领取
app.get('/activityUser/scanCode', (req, res, next) => {
    activityUser.scanCode(req, res, next);
});

// 获取热点状态
app.get('/hot/getStatus', (req, res, next) => {
    hot.getStatus(req, res, next);
});

// 获取扫一扫用户信息
app.get('/scanCode/getScanCodeUser', (req, res, next) => {
    scanCode.scanCodeUser(req, res, next);
});

app.get('/send', (req, res, next) => {
    send.send(req, res, next);
});

// 签到
app.get('/getQiandao', (req, res, next) => {
    qiandao.getQiandao(req, res, next);
});

app.get('/addQiandao', (req, res, next) => {
    qiandao.insertQiandao(req, res, next);
});

// 盲盒
app.get('/getManghe', (req, res, next) => {
    manghe.getManghe(req, res, next);
});

app.post('/updateMangheUser', (req, res, next) => {
    mangheUser.updateMangheUser(req, res, next);
});
// app.get('/setjinyong', (req, res, next) => {
//     mangheUser.setjinyong(req, res, next);
// });


// 创建一条数据
app.get('/createManghe', (req, res, next) => {
    manghe.createManghe(req, res, next);
});




app.get('/getMangheUser', (req, res, next) => {
    mangheUser.getMangheUser(req, res, next);
});

app.get('/updateMangheMessage', (req, res, next) => {
    mangheUser.updateMangheMessage(req, res, next);
});


app.get('/messageSends', (req, res, next) => {
    mangheUser.messageSends(req, res, next);
});

app.get('/updateMangheUserOrderNumberAndShareStatus', (req, res, next) => {
    mangheUser.updateMangheUserOrderNumberAndShareStatus(req, res, next);
});

// 完成浏览任务
app.get('/addOrderNumberLiulan', (req, res, next) => {
    mangheUser.addOrderNumberLiulan(req, res, next);
});
// 提现
app.get('/tixian', (req, res, next) => {
    mangheUser.tixian(req, res, next);
});


// 获取是否显示招聘信息接口
app.get('/getZhaoPinStatusurl', (req, res, next) => {
    zhaoPinStatus.getZhaoPinStatusurl(req, res, next);
});


// 临时测试新增和更新
app.get('/mangheInvitationUser/updateUser', (req, res, next) => {
    mangheInvitationUser.updateUser(req, res, next);
});

app.get('/mangheInvitationUser/getShareRanking', (req, res, next) => {
    mangheInvitationUser.getShareRanking(req, res, next);
});

// 更新招工状态

app.get('/updateStatus', (req, res, next) => {
    zhaoPinStatus.updateStatus(req, res, next);
});

// 消息推送
app.post('/', async (req, res, next) => {
    console.log('消息推送', req.body)
    res.send('success') // 不进行任何回复，直接返回success，告知微信服务器已经正常收到。
});




// // 更新计数
// app.post('/api/count', async (req, res) => {
//     const {action} = req.body;
//     if (action === 'inc') {
//         await Counter.create();
//     } else if (action === 'clear') {
//         await Counter.destroy({
//             truncate: true
//         });
//     }
//     res.send({
//         code: 0,
//         data: await Counter.count()
//     });
// });

// // 获取计数
// app.get('/api/count', async (req, res) => {
//     const result = await Counter.count();
//     res.send({
//         code: 0,
//         data: result
//     });
// });

// // 小程序调用，获取微信 Open ID
// app.get('/api/wx_openid', async (req, res) => {
//     if (req.headers['x-wx-source']) {
//         res.send(req.headers['x-wx-openid']);
//     }
// });
async function init() {
    try {
        await user.user.sync({alert: true});
        await city.city.sync({alert: true});
        await province.province.sync({alert: true});
        await area.area.sync({alert: true});
        await workType.workType.sync({alert: true});
        await recruitment.recruitment.sync({alter: true});
        await findJob.findJob.sync({alter: true});
        await market.market.sync({alter: true});
        await transactionType.transactionType.sync({alter: true});
        await shopType.shopType.sync({alter: true});
        await shop.shop.sync({alter: true});
        await conllection.conllection.sync({alter: true});
        await shenhe.shenhe.sync({alter: true});
        await telPhone.telPhone.sync({alter: true});
        await activity.activity.sync({alter: true});
        await activityUser.activityUser.sync({alter: true});
        await hot.hot.sync({alter: true});
        await scanCode.scanCode.sync({alter: true});
        await qiandao.qiandao.sync({alter: true});
        await manghe.manghe.sync({alter: true});
        await mangheUser.mangheUser.sync({alter: true});
        await zhaoPinStatus.zhaoPinStatus.sync({alter: true});
        await mangheInvitationUser.mangheInvitationUser.sync({alter: true});
        await recruitmentUserFanKui.recruitmentUserFanKui.sync({alter: true});
        await searchName.searchName.sync({alter: true});
    }
    catch (err) {
        console.log('初始化数据库失败', err);
    }
}

const port = process.env.PORT || 80;
console.log(port);
async function bootstrap() {
    try {
        await init();
    }
    catch (err) {
        console.log('err', err);
    }
    // recruitment.sync({alter: true});
    app.listen(port, () => {
        console.log('启动成功', port);
    });
}

bootstrap();
